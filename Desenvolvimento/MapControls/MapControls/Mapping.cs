﻿using MapControls.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapControls
{
    public class Mapping
    {
        public Dictionary<int, string> genSets = new Dictionary<int, string>();
        public Dictionary<string, DungeonMap> maps = new Dictionary<string, DungeonMap>();

        public Mapping(string mapInfoTxt, string generationSettingsTxt)
        {
            string val;
            JObject obj = JsonConvert.DeserializeObject(generationSettingsTxt) as JObject;
            foreach(KeyValuePair<string, JToken> item in obj)
            {
                val = item.Value.ToString();
                val = val.Replace("\r", "");
                val = val.Replace("\t", "");
                val = val.Replace("\n", "");
                val = val.Trim();
                genSets.Add(int.Parse(item.Key.ToString()), val);
            }
            mapInfoTxt = "{" + mapInfoTxt.Substring(mapInfoTxt.IndexOf("*/") + 2);
            obj = JsonConvert.DeserializeObject(mapInfoTxt) as JObject;
            foreach (KeyValuePair<string, JToken> item in obj)
            {
                maps.Add(item.Key.ToString(), new DungeonMap(item.Key.ToString(), item.Value["tiles"] as JArray, genSets[int.Parse(item.Value["generationSet"].ToString())]));
            }
        }
        
    }
}
