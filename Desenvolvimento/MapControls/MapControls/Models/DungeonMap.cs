﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapControls.Models
{
    public class DungeonMap
    {
        private string name;
        public int order;
        public List<List<int>> tileMap = new List<List<int>>();
        public List<List<int>> revealedMap;
        private string generationSet;
        private int startPositionX;
        private int startPositionY;

        public DungeonMap() { }
        public DungeonMap(string name, JArray tiles, string generationSet)
        {
            this.name = name;
            order = tiles.Count;
            this.generationSet = generationSet;
            foreach (JToken token in tiles)
            {
                List<JToken> tokens = token.ToList();
                List<int> _tiles = new List<int>();
                foreach(JToken _token in tokens)
                {
                    _tiles.Add(int.Parse(_token.ToString()));
                }
                tileMap.Add(_tiles);
            }
            revealedMap = new List<List<int>>();
            for (int y = 0; y < order; y++)
            {
                List<int> line = new List<int>();
                for (int x = 0; x < order; x++)
                {
                    line.Add(0);
                    if (tileMap[y][x] == 4)
                    {
                        startPositionX = x;
                        startPositionY = y;
                    } 
                }
                revealedMap.Add(line);
            }
        }

        public bool IsFullyRevealed()
        {
            double completion = GetCompletionRate();
            if (completion < 1)
                return 1 - completion < 0.0001;
            else if (completion > 1)
                return completion - 1 < 0.0001;
            else
                return true;
        }
        public double GetCompletionRate()
        {
            double res = 0;
            int maxTiles = (order * order);
            for (int line = 0; line < order; line++)
                for (int cell = 0; cell < order; cell++)
                    if (tileMap[line][cell] == 0)
                        maxTiles--;
            for (int line = 0; line < order; line++)
                for (int cell = 0; cell < order; cell++)
                    if (tileMap[line][cell] != 0)
                        if (revealedMap[line][cell] != 0)
                            res += 1;
            return (res / maxTiles);
        }

        /// <summary>
        /// Retorna as coordenadas iniciais do cursor.
        /// </summary>
        /// <returns>Array de dois int, sendo 0 = y; 1 = x</returns>
        public int[] GetStartPosition()
        {
            return new int[]{ startPositionY, startPositionX};
        }
        public string GetName()
        {
            return name;
        }
        /// <summary>
        /// Retorna uma seleção de inimigos dentro do generationSet configurado.
        /// </summary>
        /// <returns></returns>
        public string GenerateEntities()
        {
            int range = generationSet.Split('|').Length;
            int chosen = new Random().Next(range);
            return generationSet.Split('|')[chosen];
        }
        /// <summary>
        /// Revela todos os tiles adjacentes ao tile indicado.
        /// </summary>
        /// <param name="y">índice da coluna do tile</param>
        /// <param name="x">índice da linha do tile</param>
        public void RevealAllAdjacentTiles(int y, int x)
        {
            revealedMap[y][x] = 1;
            if (y < order - 1)
                if (tileMap[y + 1][x] != 0) revealedMap[y + 1][x] = 1;
            if (y > 0)
                if (tileMap[y - 1][x] != 0) revealedMap[y - 1][x] = 1;
            if (x < order - 1)
                if (tileMap[y][x + 1] != 0) revealedMap[y][x + 1] = 1;
            if (x > 0)
                if (tileMap[y][x - 1] != 0) revealedMap[y][x - 1] = 1;
        }
        /// <summary>
        /// Revela apenas o tile adjacente à direção
        /// </summary>
        /// <param name="y">índice da coluna do tile</param>
        /// <param name="x">índice da linha do tile</param>
        /// <param name="direction">direção do cursor</param>
        public void RevealFollowingTile(int y, int x, int direction)
        {
            revealedMap[y][x] = 1;
            switch (direction)
            {
                case 0:
                    if (x < order - 1)
                        if (tileMap[y][x + 1] != 0) revealedMap[y][x + 1] = 1;
                    break;
                case 90:
                    if (y > 0)
                        if (tileMap[y - 1][x] != 0) revealedMap[y - 1][x] = 1;
                    break;
                case 180:
                    if (x > 0)
                        if (tileMap[y][x - 1] != 0) revealedMap[y][x - 1] = 1;
                    break;
                case 270:
                    if (y < order - 1)
                        if (tileMap[y + 1][x] != 0) revealedMap[y + 1][x] = 1;
                    break;
                default: break;
            }
        }
        /// <summary>
        /// Revela todos os tiles adjacentes exceto o tile oposto à direção
        /// </summary>
        /// <param name="y">índice da coluna do tile</param>
        /// <param name="x">índice da linha do tile</param>
        /// <param name="direction">direção do cursor</param>
        public void RevealFOVTiles(int y, int x, int direction)
        {
            revealedMap[y][x] = 1;
            switch (direction)
            {
                case 0:
                    if (y < order - 1)
                        if (tileMap[y + 1][x] != 0) revealedMap[y + 1][x] = 1;
                    if (y > 0)
                        if (tileMap[y - 1][x] != 0) revealedMap[y - 1][x] = 1;
                    if (x < order - 1)
                        if (tileMap[y][x + 1] != 0) revealedMap[y][x + 1] = 1;
                    break;
                case 90:
                    if (y > 0)
                        if (tileMap[y - 1][x] != 0) revealedMap[y - 1][x] = 1;
                    if (x < order - 1)
                        if (tileMap[y][x + 1] != 0) revealedMap[y][x + 1] = 1;
                    if (x > 0)
                        if (tileMap[y][x - 1] != 0) revealedMap[y][x - 1] = 1;
                    break;
                case 180:
                    if (y < order - 1)
                        if (tileMap[y + 1][x] != 0) revealedMap[y + 1][x] = 1;
                    if (y > 0)
                        if (tileMap[y - 1][x] != 0) revealedMap[y - 1][x] = 1;
                    if (x > 0)
                        if (tileMap[y][x - 1] != 0) revealedMap[y][x - 1] = 1;
                    break;
                case 270:
                    if (y < order - 1)
                        if (tileMap[y + 1][x] != 0) revealedMap[y + 1][x] = 1;
                    if (x < order - 1)
                        if (tileMap[y][x + 1] != 0) revealedMap[y][x + 1] = 1;
                    if (x > 0)
                        if (tileMap[y][x - 1] != 0) revealedMap[y][x - 1] = 1;
                    break;
                default: break;
            }
        }
    }
}
