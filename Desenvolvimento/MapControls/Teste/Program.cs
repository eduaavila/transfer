﻿using MapControls;
using MapControls.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teste
{
    class Program
    {
        public static void Main(string[] args)
        {
            // No Unity, Resource.Load<TextAsset>(@"JSON\ARQUIVO_A_SER_LIDO.txt");
            string resourcesJsonPath = @"C:\SVN\GLEECH\Desenvolvimento\HDH\Assets\Resources\JSON\";
            string mapInfoTxt = File.ReadAllText(resourcesJsonPath + @"mapInfo.txt");
            string generationSettingsTxt = File.ReadAllText(resourcesJsonPath + @"generationSettings.txt");

            // Mapping será a instância única de mapas do Unity.
            Mapping mapping = new Mapping(mapInfoTxt, generationSettingsTxt);

            Console.WriteLine("mapping generated");
            while (true)
            {
                Console.ReadKey(true);
                Console.Clear();
                DungeonMap currentMap = mapping.maps["map1"];
                string line;
                Console.WriteLine("Full map\t\tRevealed map");

                int tileX = currentMap.GetStartPosition()[1];
                int tileY = currentMap.GetStartPosition()[0];
                currentMap.RevealFollowingTile(tileY, tileX, 0);

                for (int y=0; y < currentMap.order; y++)
                {
                    line = "";
                    for (int x = 0; x < currentMap.order; x++)
                    {
                        line += currentMap.tileMap[y][x] + ",";
                    }
                    line += "\t";
                    for (int x = 0; x < currentMap.order; x++)
                    {
                        line += currentMap.revealedMap[y][x] + ",";
                    }
                    Console.WriteLine(line);
                }

                Console.WriteLine($"\nStart position x:{tileX} y:{tileY}");
                Console.WriteLine("\nEnemies: " + currentMap.GenerateEntities());
            }
        }
    }
}
