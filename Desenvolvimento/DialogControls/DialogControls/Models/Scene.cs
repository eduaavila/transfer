﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DialogControls.Models
{
    public class Scene
    {
        private string sceneName;
        private string backgroundImage;
        private string backgroundMusic;
        public readonly List<Textbox> textboxes = new List<Textbox>();

        public Scene(string sceneName, JObject scene)
        {
            this.sceneName = sceneName;
            if(scene.HasValues)
            {
                backgroundImage = scene["background_image"].ToString();
                backgroundMusic = scene["background_music"].ToString();
                JArray boxes = scene["textboxes"] as JArray;
                foreach (JObject textbox in boxes)
                {
                    textboxes.Add(
                        new Textbox(
                            textbox["spriteLeft"].ToString(),
                            textbox["spriteRight"].ToString(),
                            textbox["activeSprite"].ToString(),
                            textbox["speaker"].ToString(),
                            textbox["text"].ToString()
                        )
                    );
                }
            }
        }
        public string GetSceneName()
        {
            return sceneName;
        }
        public string GetBackgroundImage()
        {
            return backgroundImage;
        }
        public string GetBackgroundMusic()
        {
            return backgroundMusic;
        }
    }
}
