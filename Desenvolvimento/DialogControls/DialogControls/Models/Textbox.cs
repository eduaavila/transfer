﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DialogControls.Models
{
    public class Textbox
    {
        private readonly string spriteLeft;
        private readonly string spriteRight;
        private readonly string activeSprite;
        public readonly string speaker;
        public readonly string text;

        public Textbox(string spriteLeft, string spriteRight, string activeSprite, string speaker, string text)
        {
            this.spriteLeft = spriteLeft;
            this.spriteRight = spriteRight;
            this.activeSprite = activeSprite;
            this.speaker = speaker;
            this.text = text;
        }
        public string GetSpriteLeft()
        {
            return spriteLeft;
        }
        public string GetSpriteRight()
        {
            return spriteRight;
        }
        public string GetActiveSprite()
        {
            return activeSprite;
        }
    }
}
