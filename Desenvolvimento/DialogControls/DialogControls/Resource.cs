﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DialogControls.Models;

namespace DialogControls
{
    public class Resource
    {
        private readonly string language;
        public readonly Dictionary<string, Scene> scenes = new Dictionary<string, Scene>();

        public Resource(string textScene, string language)
        {
            this.language = language;
            JObject textSceneJSON = JsonConvert.DeserializeObject(textScene) as JObject;
            JObject languageJSON = textSceneJSON[language] as JObject;

            foreach (KeyValuePair<string, JToken> scene in languageJSON)
            {
                scenes.Add(scene.Key, new Scene(scene.Key, scene.Value as JObject));
            }
        }
        public string GetLanguage()
        {
            return language;
        }
    }
}
