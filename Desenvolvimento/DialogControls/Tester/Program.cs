﻿using DialogControls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tester
{
    class Program
    {
        public static void Main(string[] args)
        {
            string allLines;
            string lang;
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Insira o caminho do arquivo dialogs.js: ");
                string path = Console.ReadLine();
                try
                {
                    allLines = File.ReadAllText(path);
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Não conseguiu ler o arquivo. " + e.Message);
                }
            }
            Console.WriteLine("Arquivo lido com sucesso.");
            while (true)
            {
                Console.WriteLine("Insira uma linguagem para carregar: ");
                lang = Console.ReadLine();
                lang = string.Join("-", new string[] { lang.Split('-')[0].ToLower(), lang.Split('-')[1].ToUpper() });
                try
                {
                    if (allLines.Contains($"\"{lang}\""))
                        break;
                    else
                        throw new Exception("Linguagem não encontrada.");
                }
                catch (Exception e)
                {
                    Console.Clear();
                    Console.WriteLine("Não conseguiu encontrar linguagem. " + e.Message);
                }
            }
            string asJson = allLines.Remove(0, allLines.IndexOf("=") + 1);
            try
            {
                Resource resx = new Resource(asJson, lang);
                Console.WriteLine($"Recursos na linguagem '{resx.GetLanguage()}' carregados");
            }
            catch(Exception e)
            {
                Console.WriteLine("Recursos na linguagem não puderam ser carregados. " + e.Message);
            }

            Console.ReadKey();
        }
    }
}
