﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverlayControls
{
    public enum TriggerType
    {
        ExploreOnStart,
        ExploreOnEnd,
        ExploreOnTile,
        ConquerOnStart,
        ConquerOnEnd,
        ConquerOnTile
    }
}
