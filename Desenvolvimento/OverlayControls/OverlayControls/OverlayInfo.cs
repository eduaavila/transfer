﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OverlayControls.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverlayControls
{
    public class OverlayInfo
    {
        public static string ResourcesAssetPath
        {
            get { return @"JSON\overlayInfo"; }                
        }
        public static string PhysicalFilePath
        {
            get { return @"C:\SVN\GLEECH\Desenvolvimento\HDH\Assets\Resources\JSON\overlayInfo.txt"; }
        }

        private JObject overlayObj = new JObject();
        private string textAsset = "";
        private string langCode = "";

        public OverlayInfo(string textAsset, string langCode)
        {
            SetTextAsset(textAsset);
            SetLanguage(langCode);
        }
        public void SetTextAsset(string textAsset)
        {
            this.textAsset = textAsset;
            overlayObj = JObject.Parse(textAsset);
        }
        public string GetTextAsset()
        {
            return textAsset;
        }
        public string GetLanguage()
        {
            return langCode;
        }
        public void SetLanguage(string langCode)
        {
            this.langCode = langCode;
        }
        public List<OverlayTextbox> GetTextboxList(PropType prop, string query)
        {
            List<OverlayTextbox> list = new List<OverlayTextbox>();
            JObject props = overlayObj["props"] as JObject;
            string propName = "";
            switch (prop)
            {
                case PropType.Chest:
                    propName = "chest";
                    break;
                case PropType.Door:
                    propName = "door";
                    break;
                case PropType.Unit:
                    propName = "unit";
                    break;
                case PropType.Boss:
                    propName = "boss";
                    break;
                default: break;
            }
            JObject innerObj = props[$"{propName}Frame"] as JObject;
            if (innerObj.ContainsKey(query))
            {
                innerObj = innerObj[query] as JObject;
                JArray boxes = innerObj["textboxes"] as JArray;
                list = JArrayToOverlayTextboxList(boxes);
            }
            return list;
        }
        public List<OverlayTextbox> GetTextboxList(TriggerType trigger, string map, int ?tileX, int ?tileY)
        {
            List<OverlayTextbox> list = new List<OverlayTextbox>();
            JObject triggerObj = overlayObj["trigger"] as JObject;
            if (!triggerObj.ContainsKey(map)) return list;
            JObject mapObj = triggerObj[map] as JObject;
            string query = "";
            string subQuery = "";
            switch (trigger)
            {
                case TriggerType.ExploreOnStart:
                    query = "ExploreScene";
                    subQuery = "onStart";
                    break;
                case TriggerType.ExploreOnEnd:
                    query = "ExploreScene";
                    subQuery = "onEnd";
                    break;
                case TriggerType.ExploreOnTile:
                    query = "ExploreScene";
                    subQuery = "onTile";
                    break;
                case TriggerType.ConquerOnStart:
                    query = "ConquerMoveScene";
                    subQuery = "onStart";
                    break;
                case TriggerType.ConquerOnEnd:
                    query = "ConquerMoveScene";
                    subQuery = "onEnd";
                    break;
                case TriggerType.ConquerOnTile:
                    query = "ConquerMoveScene";
                    subQuery = "onTile";
                    break;
            }
            JObject queriedObj = mapObj[query] as JObject;
            if(subQuery == "onTile")
            {
                JArray subObj = queriedObj[subQuery] as JArray;
                if (subObj.HasValues)
                {
                    string jsonPath = $"$[?(@.tileX == {tileX} && @.tileY == {tileY})].textboxes";
                    JToken boxes = subObj.SelectToken(jsonPath);
                    subObj = boxes as JArray;
                    if(subObj != null)
                        list = JArrayToOverlayTextboxList(subObj);
                }
            }
            else
            {
                JObject subObj = queriedObj[subQuery] as JObject;
                if (subObj.HasValues)
                {
                    JArray boxes = subObj["textboxes"] as JArray;
                    list = JArrayToOverlayTextboxList(boxes);
                }
            }
            return list;
        }

        private List<OverlayTextbox> JArrayToOverlayTextboxList(JArray textboxesArray)
        {
            List<OverlayTextbox> list = new List<OverlayTextbox>();
            foreach (JToken token in textboxesArray)
            {
                JObject tokenObj = token as JObject;
                list.Add(new OverlayTextbox(tokenObj, langCode));
            }
            return list;
        }
    }
}
