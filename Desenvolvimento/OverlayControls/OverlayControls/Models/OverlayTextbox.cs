﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverlayControls.Models
{
    public class OverlayTextbox
    {
        private string position;
        private string sprite;
        private string speaker;
        private string text;

        public OverlayTextbox(JObject textboxObj, string langCode)
        {
            position = textboxObj["position"].ToString();
            sprite = textboxObj["sprite"].ToString();
            JObject obj = textboxObj["speaker"] as JObject;
            speaker = obj[langCode].ToString();
            obj = textboxObj["text"] as JObject;
            text = obj[langCode].ToString();
        }
        public string GetPosition()
        {
            return position;
        }
        public string GetSprite()
        {
            return sprite;
        }
        public string GetSpeaker()
        {
            return speaker;
        }
        public string GetText()
        {
            return text;
        }
    }
}
