﻿using OverlayControls;
using OverlayControls.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            List<OverlayTextbox> boxes;
            string textAsset = File.ReadAllText(OverlayInfo.PhysicalFilePath);

            OverlayInfo overlayInfo = new OverlayInfo(textAsset, "pt-br");

            boxes = overlayInfo.GetTextboxList(TriggerType.ExploreOnTile, "prologue_map1", 0, 2);

            if (boxes.Count > 0)
                foreach (OverlayTextbox box in boxes)
                {
                    Console.WriteLine(box.GetText());
                    Console.ReadKey(true);
                }
            else
            {
                Console.WriteLine("No boxes loaded");
                Console.ReadKey(true);
            }   
        }
    }
}
