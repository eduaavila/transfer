﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class PauseTests
    {
        [UnityTest]
        public IEnumerator PauseTestsWithEnumeratorPasses()
        {
            HDH.PauseBehaviour pause = new GameObject().AddComponent<HDH.PauseBehaviour>();
            pause.SetPauseObjs(new GameObject[0]);

            pause.Pause();
            Assert.AreEqual(true, HDH.PauseBehaviour.IsPaused());

            pause.Unpause();
            Assert.AreEqual(false, HDH.PauseBehaviour.IsPaused());

            float[] volumeTests = new float[] { 0f, 100f };
            foreach(float volume in volumeTests){
                pause.SlideVolume(volume);
                Assert.AreEqual(volume, HDH.CrossSceneClass.GetConfigs().GetVolumeMaster());
            }

            string[] langTests = new string[] { "pt-br", "en-us" };
            foreach (string lang in langTests)
            {
                pause.ChangeLanguage(lang);
                Assert.AreEqual(lang, HDH.CrossSceneClass.GetConfigs().GetLanguage());
            }

            yield return null;
        }
    }
}
