﻿using HDH;
using OverlayControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class BossFrameBehaviour : MonoBehaviour
    {
        private void OnMouseUp()
        {
            CrossSceneClass.Log("Boss frame clicked");
            if (GameObject.FindGameObjectsWithTag("Overlay").Length > 0) return;
            Overlay.LoadPropOverlayTextboxes(PropType.Boss, "boss");
            GameObject overlayGameObject = Instantiate(Resources.Load<GameObject>("Prefabs/Overlay"), GameObject.Find("Canvas").transform);
            overlayGameObject.name = "OverlayBoss";
            overlayGameObject.transform.localPosition.Set(0, 0, 0);
        }
    }
}
