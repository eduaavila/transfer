﻿using OverlayControls;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HDH
{
    public class DoorFrameBehaviour : MonoBehaviour
    {
        private void OnMouseUp()
        {
            CrossSceneClass.Log("Door frame clicked");
            if (GameObject.FindGameObjectsWithTag("Overlay").Length > 0) return;
            Overlay.LoadPropOverlayTextboxes(PropType.Door, "common");
            GameObject overlayGameObject = Instantiate(Resources.Load<GameObject>("Prefabs/Overlay"), GameObject.Find("Canvas").transform);
            overlayGameObject.name = "OverlayDoor";
            overlayGameObject.transform.localPosition.Set(0, 0, 0);
        }
    }
}