﻿using HDH;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class DialogueTrigger : MonoBehaviour
    {
        public void ButtonClick()
        {
            CrossSceneClass.Verbose("Dialogue element triggered via button.", gameObject.name);
            DialogueBehaviour.NextTextbox();
        }
    }
}
