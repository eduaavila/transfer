﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OverlayControls;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace HDH
{
    public class ExplorationBehaviour : MonoBehaviour
    {
        private float timeLimit;

        public GameObject gameMap;
        public GameObject counter;

        private static GameObject LPath;
        private static GameObject RPath;
        private static GameObject UPath;
        private static GameObject BBtn;
        void Start()
        {
            if (!CrossSceneClass.IsLoaded())
            {
                CrossSceneClass.Log($"Save file is not loaded.");
                SceneManager.LoadScene("MainMenuScene");
                return;
            }
            PauseBehaviour.GlobalizeUI();
            timeLimit = CrossSceneClass.GetProfile().GetChapterInfo().GetExploreDuration();
            if (Overlay.TryLoadOverlayTextboxes(TriggerType.ExploreOnStart, 0, 0))
            {
                CrossSceneClass.Log("Overlay Trigger loaded", TriggerType.ExploreOnStart.ToString());
                GameObject overlayGameObject = Instantiate(Resources.Load<GameObject>("Prefabs/Overlay"), GameObject.Find("Canvas").transform);
                overlayGameObject.name = "Overlay";
                overlayGameObject.transform.localPosition.Set(0, 0, 0);
            }
            GetButtons();
            UpdateSurrounds();
            
        }
        private static void GetButtons()
        {
            LPath = GameObject.Find("LOpen");
            RPath = GameObject.Find("ROpen");
            UPath = GameObject.Find("UOpen");
            BBtn = GameObject.Find("btnBack");
        }

        public void LoadNextScene()
        {
            SceneManager.LoadScene("ConquerMoveScene");
        }

        void Update()
        {
            if (!CrossSceneClass.IsLoaded()) return;
            if (CrossSceneClass.MustFreezeCounters()) return;
            timeLimit -= Time.deltaTime;
            if (timeLimit > 0)
            {
                counter.transform.Find("seconds").GetComponent<Text>().text = Convert.ToInt32(timeLimit).ToString();
            }
            else
            {
                CrossSceneClass.Log("Explore time over");
                counter.transform.Find("seconds").GetComponent<Text>().text = "0";
                if (Overlay.TryLoadOverlayTextboxes(TriggerType.ExploreOnEnd, 0, 0))
                {
                    CrossSceneClass.Log("Overlay Trigger loaded", TriggerType.ExploreOnEnd.ToString());
                    GameObject overlayGameObject = Instantiate(Resources.Load<GameObject>("Prefabs/Overlay"), GameObject.Find("Canvas").transform);
                    overlayGameObject.name = "OverlayOnEnd";
                    overlayGameObject.transform.localPosition.Set(0, 0, 0);
                }
                else
                {
                    SceneManager.LoadScene("ConquerMoveScene");
                }
            }
        }
        public static void UpdateSurrounds()
        {
            int x = CrossSceneClass.GetCurrentMap().GetStartPosition()[1];
            int y = CrossSceneClass.GetCurrentMap().GetStartPosition()[0];
            int direction = CrossSceneClass.GetProfile().GetChapterInfo().GetStartingCursorDirection();
            int order = CrossSceneClass.GetCurrentMap().order;
            List<List<int>> tileMap = CrossSceneClass.GetCurrentMap().tileMap;
            switch (direction)
            {
                case 0:
                    if (x == 0)
                        BBtn.SetActive(false);
                    else
                        BBtn.SetActive(true);
                    if (x == order - 1)
                        UPath.SetActive(false);
                    else
                        UPath.SetActive(true);
                    if (y == 0)
                        LPath.SetActive(false);
                    else
                        LPath.SetActive(true);
                    if (y == order - 1)
                        RPath.SetActive(false);
                    else
                        RPath.SetActive(true);
                    if (y < order - 1)
                    {
                        if (tileMap[y + 1][x] == 0)
                            RPath.SetActive(false);
                        else
                            RPath.SetActive(true);
                    }
                    if (y > 0)
                    {
                        if (tileMap[y - 1][x] == 0)
                            LPath.SetActive(false);
                        else
                            LPath.SetActive(true);
                    }
                    if (x < order - 1)
                    {
                        if (tileMap[y][x + 1] == 0)
                            UPath.SetActive(false);
                        else
                            UPath.SetActive(true);
                    }
                    if (x > 0)
                    {
                        if (tileMap[y][x - 1] == 0)
                            BBtn.SetActive(false);
                        else
                            BBtn.SetActive(true);
                    }
                    break;
                case 90:
                    if (x == 0)
                        LPath.SetActive(false);
                    else
                        LPath.SetActive(true);
                    if (x == order - 1)
                        RPath.SetActive(false);
                    else
                        RPath.SetActive(true);
                    if (y == 0)
                        UPath.SetActive(false);
                    else
                        UPath.SetActive(true);
                    if (y == order - 1)
                        BBtn.SetActive(false);
                    else
                        BBtn.SetActive(true);
                    if (x > 0)
                    {
                        if (tileMap[y][x - 1] == 0)
                            LPath.SetActive(false);
                        else
                            LPath.SetActive(true);
                    }
                    if (y > 0)
                    {
                        if (tileMap[y - 1][x] == 0)
                            UPath.SetActive(false);
                        else
                            UPath.SetActive(true);
                    }
                    if (x < order - 1)
                    {
                        if (tileMap[y][x + 1] == 0)
                            RPath.SetActive(false);
                        else
                            RPath.SetActive(true);
                    }
                    if (y < order - 1)
                    {
                        if (tileMap[y + 1][x] == 0)
                            BBtn.SetActive(false);
                        else
                            BBtn.SetActive(true);
                    }
                    break;
                case 180:
                    if (x == 0)
                        UPath.SetActive(false);
                    else
                        UPath.SetActive(true);
                    if (x == order - 1)
                        BBtn.SetActive(false);
                    else
                        BBtn.SetActive(true);
                    if (y == 0)
                        RPath.SetActive(false);
                    else
                        RPath.SetActive(true);
                    if (y == order - 1)
                        LPath.SetActive(false);
                    else
                        LPath.SetActive(true);
                    if (y < order - 1)
                    {
                        if (tileMap[y + 1][x] == 0)
                            LPath.SetActive(false);
                        else
                            LPath.SetActive(true);
                    }
                    if (y > 0)
                    {
                        if (tileMap[y - 1][x] == 0)
                            RPath.SetActive(false);
                        else
                            RPath.SetActive(true);
                    }
                    if (x > 0)
                    {
                        if (tileMap[y][x - 1] == 0)
                            UPath.SetActive(false);
                        else
                            UPath.SetActive(true);
                    }
                    if(x < order - 1)
                    {
                        if (tileMap[y][x + 1] == 0)
                            BBtn.SetActive(false);
                        else
                            BBtn.SetActive(true);
                    }
                    break;
                case 270:
                    if (x == 0)
                        RPath.SetActive(false);
                    else
                        RPath.SetActive(true);
                    if (x == order - 1)
                        LPath.SetActive(false);
                    else
                        LPath.SetActive(true);
                    if (y == 0)
                        BBtn.SetActive(false);
                    else
                        BBtn.SetActive(true);
                    if (y == order - 1)
                        UPath.SetActive(false);
                    else
                        UPath.SetActive(true);
                    if (x > 0)
                    {
                        if (tileMap[y][x - 1] == 0)
                            RPath.SetActive(false);
                        else
                            RPath.SetActive(true);
                    }
                    if (y < order - 1)
                    {
                        if (tileMap[y + 1][x] == 0)
                            UPath.SetActive(false);
                        else
                            UPath.SetActive(true);
                    }
                    if (x < order - 1)
                    {
                        if (tileMap[y][x + 1] == 0)
                            LPath.SetActive(false);
                        else
                            LPath.SetActive(true);
                    }
                    if (y > 0)
                    {
                        if (tileMap[y - 1][x] == 0)
                            BBtn.SetActive(false);
                        else
                            BBtn.SetActive(true);
                    }
                    break;
                default: break;
            }
        }
        public static void UpdateSurrounds(int x, int y, int direction)
        {
            int order = CrossSceneClass.GetCurrentMap().order;
            List<List<int>> tileMap = CrossSceneClass.GetCurrentMap().tileMap;
            switch (direction)
            {
                case 0:
                    if (x == 0)
                        BBtn.SetActive(false);
                    else
                        BBtn.SetActive(true);
                    if (x == order - 1)
                        UPath.SetActive(false);
                    else
                        UPath.SetActive(true);
                    if (y == 0)
                        LPath.SetActive(false);
                    else
                        LPath.SetActive(true);
                    if (y == order - 1)
                        RPath.SetActive(false);
                    else
                        RPath.SetActive(true);
                    if (y < order - 1)
                    {
                        if (tileMap[y + 1][x] == 0)
                            RPath.SetActive(false);
                        else
                            RPath.SetActive(true);
                    }
                    if (y > 0)
                    {
                        if (tileMap[y - 1][x] == 0)
                            LPath.SetActive(false);
                        else
                            LPath.SetActive(true);
                    }
                    if (x < order - 1)
                    {
                        if (tileMap[y][x + 1] == 0)
                            UPath.SetActive(false);
                        else
                            UPath.SetActive(true);
                    }
                    if (x > 0)
                    {
                        if (tileMap[y][x - 1] == 0)
                            BBtn.SetActive(false);
                        else
                            BBtn.SetActive(true);
                    }
                    break;
                case 90:
                    if (x == 0)
                        LPath.SetActive(false);
                    else
                        LPath.SetActive(true);
                    if (x == order - 1)
                        RPath.SetActive(false);
                    else
                        RPath.SetActive(true);
                    if (y == 0)
                        UPath.SetActive(false);
                    else
                        UPath.SetActive(true);
                    if (y == order - 1)
                        BBtn.SetActive(false);
                    else
                        BBtn.SetActive(true);
                    if (x > 0)
                    {
                        if (tileMap[y][x - 1] == 0)
                            LPath.SetActive(false);
                        else
                            LPath.SetActive(true);
                    }
                    if (y > 0)
                    {
                        if (tileMap[y - 1][x] == 0)
                            UPath.SetActive(false);
                        else
                            UPath.SetActive(true);
                    }
                    if (x < order - 1)
                    {
                        if (tileMap[y][x + 1] == 0)
                            RPath.SetActive(false);
                        else
                            RPath.SetActive(true);
                    }
                    if (y < order - 1)
                    {
                        if (tileMap[y + 1][x] == 0)
                            BBtn.SetActive(false);
                        else
                            BBtn.SetActive(true);
                    }
                    break;
                case 180:
                    if (x == 0)
                        UPath.SetActive(false);
                    else
                        UPath.SetActive(true);
                    if (x == order - 1)
                        BBtn.SetActive(false);
                    else
                        BBtn.SetActive(true);
                    if (y == 0)
                        RPath.SetActive(false);
                    else
                        RPath.SetActive(true);
                    if (y == order - 1)
                        LPath.SetActive(false);
                    else
                        LPath.SetActive(true);
                    if (y < order - 1)
                    {
                        if (tileMap[y + 1][x] == 0)
                            LPath.SetActive(false);
                        else
                            LPath.SetActive(true);
                    }
                    if (y > 0)
                    {
                        if (tileMap[y - 1][x] == 0)
                            RPath.SetActive(false);
                        else
                            RPath.SetActive(true);
                    }
                    if (x > 0)
                    {
                        if (tileMap[y][x - 1] == 0)
                            UPath.SetActive(false);
                        else
                            UPath.SetActive(true);
                    }
                    if (x < order - 1)
                    {
                        if (tileMap[y][x + 1] == 0)
                            BBtn.SetActive(false);
                        else
                            BBtn.SetActive(true);
                    }
                    break;
                case 270:
                    if (x == 0)
                        RPath.SetActive(false);
                    else
                        RPath.SetActive(true);
                    if (x == order - 1)
                        LPath.SetActive(false);
                    else
                        LPath.SetActive(true);
                    if (y == 0)
                        BBtn.SetActive(false);
                    else
                        BBtn.SetActive(true);
                    if (y == order - 1)
                        UPath.SetActive(false);
                    else
                        UPath.SetActive(true);
                    if (x > 0)
                    {
                        if (tileMap[y][x - 1] == 0)
                            RPath.SetActive(false);
                        else
                            RPath.SetActive(true);
                    }
                    if (y < order - 1)
                    {
                        if (tileMap[y + 1][x] == 0)
                            UPath.SetActive(false);
                        else
                            UPath.SetActive(true);
                    }
                    if (x < order - 1)
                    {
                        if (tileMap[y][x + 1] == 0)
                            LPath.SetActive(false);
                        else
                            LPath.SetActive(true);
                    }
                    if (y > 0)
                    {
                        if (tileMap[y - 1][x] == 0)
                            BBtn.SetActive(false);
                        else
                            BBtn.SetActive(true);
                    }
                    break;
                default: break;
            }
        }
    }
}