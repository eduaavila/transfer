﻿using HDH;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    private static bool allowChanges = true;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        CrossSceneClass.Verbose("Enemy frame clicked");
        if (Input.GetMouseButtonDown(0) && allowChanges)
        {
            CrossSceneClass.Log("Enemy clicked to change focus. Disabling focus changing for 2 seconds", gameObject.name);
            ConquestBattleBehaviour.ChangeTarget(gameObject);
            allowChanges = false;
            Invoke("ReallowChanges", 2f);
        }
    }

    private void ReallowChanges()
    {
        allowChanges = true;
        CrossSceneClass.Verbose("Reallow changes called");
        ConquestBattleBehaviour.ReenableFocus();
    }
    public static bool IsAllowingFocusChange()
    {
        return allowChanges;
    }
}
