﻿using HDH;
using OverlayControls;
using OverlayControls.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class ChestFrameBehaviour : MonoBehaviour
    {
        private void OnMouseUp()
        {
            CrossSceneClass.Log("Chest frame clicked");
            if (GameObject.FindGameObjectsWithTag("Overlay").Length > 0) return;
            string query = MapBehaviour.GetThisChest().GetChestType();
            Overlay.LoadPropOverlayTextboxes(PropType.Chest, query);
            GameObject overlayGameObject = Instantiate(Resources.Load<GameObject>("Prefabs/Overlay"), GameObject.Find("Canvas").transform);
            overlayGameObject.name = "Overlay";
            overlayGameObject.transform.localPosition.Set(0, 0, 0);
        }
    }
}
