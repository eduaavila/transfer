﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace HDH
{
    public class TransitionFadeBehaviour : MonoBehaviour
    {
        private float duration;
        private float currentLifetime;
        private Image image;
        private float opacity;
        private string sceneToLoad;
        private float transitionSpeed;
        // Start is called before the first frame update
        void Start()
        {
            duration = 0.1f;
            image = GetComponent<Image>();
            opacity = 0.1f;
            DefineAlpha(opacity);
            currentLifetime = 0f;
        }

        // Update is called once per frame
        void Update()
        {
            currentLifetime += (transitionSpeed + (Time.deltaTime/1000));
            opacity = currentLifetime / duration;
            Debug.Log("Current opacity: " + opacity);
            DefineAlpha(opacity);
            if (opacity >= 0.99f)
            {
                if (sceneToLoad != "")
                {
                    Debug.Log("FadeTransition ended, loading next scene!");
                    //loading da próxima cena aqui ex: SceneManager.LoadScene("sceneToLoad");
                }
                else
                {
                    Debug.Log("FadeTransition ended, but there is no scene to load, destroying itself!");
                    DestroyImmediate(this.gameObject);
                }
            }
        }
        public void SetDuration(float duration)
        {
            Debug.Log("Duration Altered to:" + duration);
            this.duration = duration;
        }
        public void SetInitialOpacity(float opacity)
        {
            Debug.Log("Opacity Altered");
            this.opacity = opacity;
        }
        private void DefineAlpha(float factor)
        {
            var tempColor = image.color;
            tempColor.a = factor;
            image.color = tempColor;
        }
        public void DefineSceneToLoad(string sceneToLoad)
        {
            this.sceneToLoad = sceneToLoad;
        }
        public void SetTransitionSpeed(float transitionSpeed)
        {
            this.transitionSpeed = transitionSpeed;
        }
    }
}