﻿using System.Collections.Generic;
using UnityEngine;
using DialogControls;
using DialogControls.Models;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace HDH
{
    public class DialogueBehaviour : MonoBehaviour
    {
        // Instanced
        private static GameObject contentText;
        private static GameObject speaker;
        private static GameObject spriteLeft;
        private static GameObject spriteRight;
        private static string scene;

        // On runtime
        private static List<Textbox> loadedBoxes;
        private static int boxIndex = 0;
        private static int writingIndex = 0;
        private static string sprLeft, sprRight;

        private static string text = "";

        // Start is called before the first frame update
        void Start()
        {
            if(!CrossSceneClass.IsLoaded())
            {
                CrossSceneClass.Log($"File is not loaded.");
                SceneManager.LoadScene("MainMenuScene");
                return;
            }
            contentText = GameObject.Find("ContentText");
            speaker = GameObject.Find("Speaker");
            spriteLeft = GameObject.Find("SpriteLeft");
            spriteRight = GameObject.Find("SpriteRight");
            boxIndex = 0;
            writingIndex = 0;
            sprLeft = "";
            sprRight = "";
            string language = CrossSceneClass.GetConfigs().GetLanguage();
            scene = CrossSceneClass.GetProfile().GetChapterInfo().GetStartingDialog();
            
            TextAsset filetxt = Resources.Load<TextAsset>(@"JSON/dialogues");

            Resource dialogue = new Resource(filetxt.text, language);
            DialogControls.Models.Scene loadedScene = dialogue.scenes[scene];
            loadedBoxes = loadedScene.textboxes;
            boxIndex = 0;

            Image sprSpriteLeft = spriteLeft.GetComponent<Image>();
            Image sprSpriteRight = spriteRight.GetComponent<Image>();

            string bgName = loadedScene.GetBackgroundImage();
            CrossSceneClass.Verbose("loadedScene.GetBackgroundImage()", bgName);
            if (bgName == "")
                bgName = "blackscreen";

            Sprite sprite = Resources.Load<Sprite>("Sprites/VN/Background/" + bgName);
            CrossSceneClass.Verbose("Load Sprite", sprite.name);

            GameObject.Find("DialogueBg").GetComponent<Image>().sprite = sprite;
            CrossSceneClass.Log("DialogueBg changed", GameObject.Find("DialogueBg").GetComponent<Image>().sprite.name);

            Text txtSpeaker = speaker.GetComponent<Text>();
            Text txtContentText = contentText.GetComponent<Text>();
            txtContentText.text = "";

            txtSpeaker.text = loadedBoxes[boxIndex].speaker;
            text = loadedBoxes[boxIndex].text;

            sprLeft = @"Sprites/VN/Character/" + loadedBoxes[boxIndex].GetSpriteLeft();
            sprRight = @"Sprites/VN/Character/" + loadedBoxes[boxIndex].GetSpriteRight();

            sprSpriteLeft.sprite = Resources.Load<Sprite>(sprLeft);
            sprSpriteRight.sprite = Resources.Load<Sprite>(sprRight);

            if(sprSpriteRight.sprite == null)
            {
                sprSpriteRight.color = new Color(0,0,0,0);
            }
            if (sprSpriteLeft.sprite == null)
            {
                sprSpriteLeft.color = new Color(0, 0, 0, 0);
            }
            AudioSource audioSrc = Camera.main.GetComponent<AudioSource>();
            audioSrc.clip = Resources.Load<AudioClip>(@"Sounds/BGM/" + loadedScene.GetBackgroundMusic());
            audioSrc.Play();
            CrossSceneClass.Verbose("DialogueBehaviour Start() finished");
        }
        public void SkipDialogs()
        {
            if (scene == "ending")
                SceneManager.LoadScene("MainMenuScene");
            else
                CrossSceneClass.Cross();
        }
        // Update is called once per frame
        void Update()
        {
            if (writingIndex < text.Length && !PauseBehaviour.IsPaused())
            {
                Text txtContentText = contentText.GetComponent<Text>();
                txtContentText.text += text[writingIndex];
                writingIndex++;
            }
            
        }
        public static void NextTextbox()
        {
            boxIndex++;
            CrossSceneClass.Verbose("Next box", $"{boxIndex}/{loadedBoxes.Count}");
            if (boxIndex < loadedBoxes.Count)
            {
                Text txtSpeaker = speaker.GetComponent<Text>();
                Text txtContentText = contentText.GetComponent<Text>();
                Image sprSpriteLeft = spriteLeft.GetComponent<Image>();
                Image sprSpriteRight = spriteRight.GetComponent<Image>();

                txtSpeaker.text = loadedBoxes[boxIndex].speaker;
                text = loadedBoxes[boxIndex].text;
                txtContentText.text = "";
                writingIndex = 0;

                sprLeft = loadedBoxes[boxIndex].GetSpriteLeft();
                sprRight = loadedBoxes[boxIndex].GetSpriteRight();

                if (sprLeft != "")
                {
                    if (sprSpriteLeft.sprite)
                    {
                        if (sprSpriteLeft.sprite.name != loadedBoxes[boxIndex].GetSpriteLeft())
                        {
                            sprLeft = @"Sprites/VN/Character/" + sprLeft;
                            Sprite spr = Resources.Load<Sprite>(sprLeft);
                            sprSpriteLeft.sprite = spr;
                        }
                    }
                    else
                    {
                        sprLeft = @"Sprites/VN/Character/" + sprLeft;
                        Sprite spr = Resources.Load<Sprite>(sprLeft);
                        sprSpriteLeft.sprite = spr;
                    }
                }

                if (sprRight != "")
                {
                    if (sprSpriteRight.sprite)
                    {
                        if (sprSpriteRight.sprite.name != loadedBoxes[boxIndex].GetSpriteRight())
                        {
                            sprRight = @"Sprites/VN/Character/" + sprRight;
                            Sprite spr = Resources.Load<Sprite>(sprRight);
                            sprSpriteRight.sprite = spr;
                        }
                    }
                    else
                    {
                        sprRight = @"Sprites/VN/Character/" + sprRight;
                        Sprite spr = Resources.Load<Sprite>(sprRight);
                        sprSpriteRight.sprite = spr;
                    }
                }
                if (sprSpriteRight.sprite == null || sprRight == "")
                {
                    sprSpriteRight.color = new Color(0, 0, 0, 0);
                }
                else
                {
                    sprSpriteRight.color = new Color(255, 255, 255, 255);
                }
                if (sprSpriteLeft.sprite == null || sprLeft == "")
                {
                    sprSpriteLeft.color = new Color(0, 0, 0, 0);
                }
                else
                {
                    sprSpriteLeft.color = new Color(255, 255, 255, 255);
                }
            }
            else
            {
                CrossSceneClass.Verbose("boxIndex exceeded limit.");
                if (scene == "ending")
                    SceneManager.LoadScene("MainMenuScene");
                else
                    CrossSceneClass.Cross();
            }
        }
    }
}