﻿using HDH;
using OverlayControls;
using OverlayControls.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace HDH
{
    public static class Overlay
    {
        private static OverlayInfo overlayInfo;
        private static List<string> playedOnce;
        private static List<OverlayTextbox> overlayTextboxes;

        public static void LoadOverlay(string langCode)
        {
            string overlayJSON = Resources.Load<TextAsset>(OverlayInfo.ResourcesAssetPath).text;
            overlayInfo = new OverlayInfo(overlayJSON, langCode);
            playedOnce = new List<string>();
        }

        public static void SetLanguage(string langCode)
        {
            overlayInfo.SetLanguage(langCode);
        }
        public static bool TryLoadOverlayTextboxes(TriggerType triggerType, int tileX, int tileY)
        {
            bool loaded;
            string map = CrossSceneClass.GetProfile().GetChapterInfo().GetStartingMap();
            overlayTextboxes = overlayInfo.GetTextboxList(triggerType, map, tileX, tileY);
            if (loaded = overlayTextboxes.Count > 0)
                CrossSceneClass.Log("Loaded overlay textboxes", $"Count:{overlayTextboxes.Count}; Map:{map} on trigger:{triggerType} at X:{tileX} Y:{tileY}");
            return loaded;
        }
        public static bool CanLoadTriggerTextboxes(TriggerType triggerType, int tileX, int tileY)
        {   
            string key = triggerType.ToString();
            string val = "";
            if (key.EndsWith("OnTile"))
                val = $"{tileX},{tileY}";
            key = CrossSceneClass.GetProfile().GetChapterInfo().GetStartingMap() + "_" + key + val;
            if(!playedOnce.Exists(x => x == key))
            {
                if (TryLoadOverlayTextboxes(triggerType, tileX, tileY))
                {
                    CrossSceneClass.Verbose("Playing TriggerTextboxes key once", key);
                    playedOnce.Add(key);
                    return true;
                }
                else
                {
                    CrossSceneClass.Verbose("No TriggerTextboxes to play", key);
                }
            }
            else
            {
                CrossSceneClass.Verbose("TriggerTextboxes key already played once or does not exists", key);
            }
            return false;
        }
        public static void LoadPropOverlayTextboxes(PropType prop, string query)
        {
            overlayTextboxes = overlayInfo.GetTextboxList(prop, query);
        }
        public static List<OverlayTextbox> GetCurrentOverlayTextboxes()
        {
            return overlayTextboxes;
        }
    }
}
