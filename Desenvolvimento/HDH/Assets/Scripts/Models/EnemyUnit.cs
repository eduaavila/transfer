﻿using HDH;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Models
{
    public class EnemyUnit
    {
        private readonly int posX;
        private readonly int posY;
        private bool alive;
        private readonly List<Enemy> enemies;
        private readonly bool isBossFight;
        private string unitSprite;

        public EnemyUnit(int posX, int posY, bool isBossFight=false)
        {
            alive = true;
            this.posX = posX;
            this.posY = posY;
            this.isBossFight = isBossFight;
            enemies = new List<Enemy>();
            unitSprite = "";
            if (isBossFight)
            {
                enemies.Add(GetMapBoss());
            }
        }

        public void AddEnemy(Enemy enemy)
        {
            enemies.Add(enemy);
            if(unitSprite == "")
                unitSprite = "dgSprite_" + enemy.GetEnemyCode();
        }
        public string GetUnitSpriteName()
        {
            return unitSprite;
        }

        public List<Enemy> GetEnemies()
        {
            return enemies;
        }
        public Enemy GetMapBoss()
        {
            string bossInfo = Resources.Load<TextAsset>(@"JSON/bossInfo").text;
            JObject bossObj = JsonConvert.DeserializeObject(bossInfo) as JObject;
            CrossSceneClass.Verbose("BossInfo read", bossObj.ToString().Replace("\n", ""));
            string mapName = CrossSceneClass.GetProfile().GetChapterInfo().GetStartingMap();
            CrossSceneClass.Verbose("Caught map name", mapName);
            bossObj = bossObj[mapName] as JObject;
            CrossSceneClass.Verbose("BossInfo inner info read", bossObj.ToString());
            string enemyCode = bossObj["enemyCode"].ToString() + new string('*', int.Parse(bossObj["rank"].ToString()));
            CrossSceneClass.Log("bossCode generated", enemyCode);
            Enemy boss = new Enemy(enemyCode);
            boss.SetBossStats();

            return boss;
        }

        public bool IsAlive()
        {
            return alive;
        }
        public void SetAlive(bool alive)
        {
            this.alive = alive;
        }
        public int GetPosX()
        {
            return posX;
        }
        public int GetPosY()
        {
            return posY;
        }
        public bool AllEnemiesDead()
        {
            return !enemies.Exists(x => x.IsAlive());
        }
        public bool IsBossFight()
        {
            return isBossFight;
        }
    }
}
