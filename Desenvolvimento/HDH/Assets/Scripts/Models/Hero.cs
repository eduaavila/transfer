﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using UnityEngine;
using HDH;

namespace Assets.Scripts.Models
{
    public class Hero : BattleEntity
    {
        private int loyalty;
        private readonly string type;
        public Hero(string type)
        {
            this.type = type.ToLower();
            switch (this.type)
            {
                case "classic":
                    baseHp = 80f;
                    baseAtk = 12f;
                    baseDef = 6f;
                    baseSpeed = 30f;
                    break;
                default:
                    CrossSceneClass.Log("Hero type not found", this.type);
                    Application.Quit();
                    break;
            }
            hp = baseHp;
            atk = baseAtk;
            def = baseDef;
            speed = baseSpeed;
        }
        private bool DecodeRequirement(string req, float completion)
        {
            string[] split = req.Split(';');
            string atr  = split[0];
            string comp = split[1];
            string val  = split[2];

            switch (atr.ToUpper())
            {
                case "HP": return CompareRequirement((hp*100/baseHp), comp, float.Parse(val));
                case "MAP": return CompareRequirement(completion, comp, float.Parse(val));
            }
            return false;
        }

        private bool CompareRequirement(float real, string comp, float expected)
        {
            CrossSceneClass.Verbose("Comparing requirements", $"{real} {comp} {expected}");
            switch (comp)
            {
                case ">": return real > expected;
                case "<": return real < expected;
                case ">=": return real >= expected;
                case "<=": return real <= expected;
                case "=": return real == expected;
            }
            return false;
        }
        public void UpdateLoyalty(float completion)
        {
            TextAsset filetxt = Resources.Load<TextAsset>(@"JSON/heroTypes");
            JObject labels = (JsonConvert.DeserializeObject(filetxt.text) as JObject)[type] as JObject;
            int loyaltyCalc = 40;
            if(DecodeRequirement(labels["requirements"]["req1"].ToString(), completion)) loyaltyCalc += 22;
            if(DecodeRequirement(labels["requirements"]["req2"].ToString(), completion)) loyaltyCalc += 18;
            if(DecodeRequirement(labels["requirements"]["req3"].ToString(), completion)) loyaltyCalc += 15;
            loyalty = loyaltyCalc;
        }
        public bool IsLoyal()
        {
            int rand = UnityEngine.Random.Range(0, 101);
            CrossSceneClass.Log("Check IsLoyal()", "Random num = "+rand+", Lealdade Atual = "+ GetLoyalty());
            return rand <= GetLoyalty();
        }
        public int GetLoyalty()
        {
            return loyalty;
        }
        public string GetHeroType()
        {
            return type;
        }
    }
}