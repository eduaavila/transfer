﻿using HDH;
using OverlayControls;
using System;
using System.IO;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Models
{
    public class Configs
    {
        private string language;
        private float volumeMaster;
        private readonly string filePath;

        public Configs()
        {
            filePath = Application.persistentDataPath + "/gameCache.txt";
            CrossSceneClass.Log("Set configs path", filePath);
            if (File.Exists(filePath))
            {
                CrossSceneClass.Log("Configs file found");
                try
                {
                    string content = File.ReadAllText(filePath, Encoding.UTF8);
                    CrossSceneClass.Log("Configs string", content);
                    foreach (string config in content.Split(';'))
                    {
                        switch (config.Split(':')[0])
                        {
                            case "language": language = config.Split(':')[1]; break;
                            case "volumeMaster": volumeMaster = float.Parse(config.Split(':')[1]); break;
                            default: CrossSceneClass.Log($"Parameter unknown.", config); break;
                        }
                    }
                    CrossSceneClass.Log("Configs file read.");
                }
                catch (Exception ex)
                {
                    CrossSceneClass.Log("Failed reading configs file.", ex.Message);
                }
            }
            else
            {
                CrossSceneClass.Log("Opening on default settings");
                language = "pt-br";
                volumeMaster = 1f;
                SaveConfigs();
            }
            Overlay.LoadOverlay(language);
        }

        public void SaveConfigs()
        {
            try
            {
                string toSave = $"language:{language};volumeMaster:{volumeMaster}";
                File.WriteAllText(filePath, toSave, Encoding.UTF8);
                CrossSceneClass.Log("Saved configs successfully.");
            }
            catch (Exception ex)
            {
                CrossSceneClass.Log("Failed saving configs.", ex.Message);
            }
        }

        public void SetLanguage(string language)
        {
            this.language = language;
            Overlay.SetLanguage(language);
        }
        public void SetVolumeMaster(float volumeMaster)
        {
            this.volumeMaster = volumeMaster;
        }
        public string GetLanguage()
        {
            return language;
        }
        public float GetVolumeMaster()
        {
            return volumeMaster;
        }
    }
}
