﻿using HDH;
using System;
using System.IO;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Models
{
    public class PlayerProfile
    {
        private ChapterInfo chapterInfo;
        private Hero hero;
        public PlayerProfile()
        {
            CrossSceneClass.Verbose("new PlayerProfile()");
            chapterInfo = new ChapterInfo();
            hero = new Hero("Classic");
        }
        public bool IsEmpty()
        {
            return chapterInfo.IsFromFile();
        }
        public string SaveFile()
        {
            try
            {
                string path = Application.persistentDataPath + "/gamesave.txt";
                var byteStream = Encoding.UTF8.GetBytes(chapterInfo.GetChapterName());
                string toSave = Convert.ToBase64String(byteStream);
                if (!File.Exists(path))
                {
                    var a = File.CreateText(path);
                    a.Close();
                    CrossSceneClass.Log("gamesave created");
                }
                File.WriteAllText(path, toSave, Encoding.UTF8);
                CrossSceneClass.Log("File saved");
                return string.Join(";", toSave);
            }
            catch(Exception ex)
            {
                CrossSceneClass.Log("Error loading profile.", ex.Message + "\n" + ex.StackTrace);
                return "Error";
            }
        }
        public ChapterInfo GetChapterInfo()
        {
            return chapterInfo;
        }
        public void SetChapterInfo(ChapterInfo chapterInfo)
        {
            this.chapterInfo = chapterInfo;
        }
        public Hero GetHero()
        {
            return hero;
        }
        public void SetHero(Hero hero)
        {
            this.hero = hero;
        }
    }
}
