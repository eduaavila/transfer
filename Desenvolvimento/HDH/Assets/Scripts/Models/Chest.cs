﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Assets.Scripts.Models
{
    public class Chest
    {
        private string chestType;
        private List<string> contentCodes;
        private int posX;
        private int posY;
        public Chest(JObject chestInfo, int posX, int posY)
        {
            HDH.CrossSceneClass.Log("Instancing Chest");
            chestType = chestInfo["chestType"].ToString();
            JArray rawContent = chestInfo["content"] as JArray;
            contentCodes = new List<string>();
            foreach(JToken content in rawContent)
            {
                contentCodes.Add(content.ToString());
            }
            this.posX = posX;
            this.posY = posY;
            HDH.CrossSceneClass.Log("Chest instanced.", $"{string.Join(",", contentCodes)} at X:{posX} Y:{posY}, type:{chestType}");
        }
        public string GetChestType()
        {
            return chestType;
        }
        public void SetChestType(string chestType)
        {
            this.chestType = chestType;
        }
        public List<string> GetContentCodes()
        {
            return contentCodes;
        }
        public void SetContentCodes(List<string> contentCodes)
        {
            this.contentCodes = contentCodes;
        }
        public int GetPosX()
        {
            return posX;
        }
        public void SetPosX(int posX)
        {
            this.posX = posX;
        }
        public int GetPosY()
        {
            return posY;
        }
        public void SetPosY(int posY)
        {
            this.posY = posY;
        }
    }
}
