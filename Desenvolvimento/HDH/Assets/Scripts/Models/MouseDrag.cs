﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Models
{
    public class MouseDrag : MonoBehaviour
    {
        private readonly float dragSpeed = 100f;
        private readonly float maxSpeed = 1000f;
        Vector3 lastMousePos;

        void OnMouseDown()
        {
            lastMousePos = Input.mousePosition;
        }
        void OnMouseDrag()
        {
            Vector3 delta = Input.mousePosition - lastMousePos;
            Vector3 force = delta * dragSpeed;
            GetComponent<Rigidbody2D>().velocity = Vector3.ClampMagnitude(force, maxSpeed);
            lastMousePos = Input.mousePosition;
        }
    }
}
