﻿using HDH;
using System;
using UnityEngine;

namespace Assets.Scripts.Models
{
    public class Enemy : BattleEntity
    {
        private readonly string name = "";
        private readonly string enemyCode;
        private readonly GameObject battlePrefab;
        private bool dead = false;
        public Enemy(string enemyCode)
        {
            this.enemyCode = enemyCode.ToLower().Substring(0, enemyCode.IndexOf("*"));
            int rank = enemyCode.Length - enemyCode.IndexOf("*");
            string prefabName = this.enemyCode + rank;
            CrossSceneClass.Verbose("enemyCode prepared", $"enemyCode:{this.enemyCode}; rank:{rank}; prefabName:{prefabName}");
            battlePrefab = Resources.Load<GameObject>($@"Prefabs/{prefabName}");
            if(battlePrefab == null)
            {
                battlePrefab = Resources.Load<GameObject>($@"Prefabs/{this.enemyCode}1");
                CrossSceneClass.Verbose("BattlePrefab not found, so first loaded.", $"prefabName:{this.enemyCode}1");
            }
            else
            {
                CrossSceneClass.Verbose("BattlePrefab loaded.", $"prefabName:{prefabName}");
            }
            sp = 0;
            switch (this.enemyCode)
            {
                case "wolf":
                    SetBaseStats(16, 3, 12.5f, 20, rank);
                    break;
                case "easter-egg":
                    SetBaseStats(10000, 10, 70, 1, 6);
                    break;
                default:
                    throw new InvalidOperationException("Invalid enemyCode");
            }
            name = CrossSceneClass.GetGlobalizedText(this.enemyCode);
            CrossSceneClass.Log("Generated enemy.", name + new string('*', rank));
        }

        public void SetBossStats()
        {
            battlePrefab.name += "_boss";
            Vector3 bossVector = battlePrefab.transform.localScale;
            bossVector.Set(bossVector.x * 1.5f, bossVector.y * 1.5f, 1);
            battlePrefab.transform.localScale = bossVector;
            bossVector = battlePrefab.transform.localPosition;
            bossVector.Set(bossVector.x, bossVector.y + 0.5f, bossVector.z);
            battlePrefab.transform.localPosition = bossVector;
            baseHp *= 1.75f;
            baseAtk *= 1.2f;
            baseDef *= 1.25f;
            baseSpeed *= 0.75f;
            hp = baseHp;
            atk = baseAtk;
            def = baseDef;
            speed = baseSpeed;

            CrossSceneClass.Verbose("Enemy set with boss stats", $"HP:{baseHp} Atk:{baseAtk} Def:{baseDef} Speed:{baseSpeed}");
        }

        public string GetEnemyCode()
        {
            return enemyCode;
        }
        public GameObject GetBattlePrefab()
        {
            return battlePrefab;
        }
        private void SetBaseStats(float hp, float def, float atk, float speed, int rank)
        {
            float modifier = 1f + (0.35f * (rank-1));
            baseHp = hp * modifier;
            baseDef = def * modifier;
            baseAtk = atk * modifier;
            baseSpeed = speed * modifier;

            this.hp = baseHp;
            this.def = baseDef;
            this.atk = baseAtk;            
            this.speed = baseSpeed;
        }
        public string GetName()
        {
            return name;
        }
        public bool IsDead()
        {
            return dead;
        }
        public void SetAsDead(bool isDead)
        {
            dead = isDead;
        }
        public static string GenerateEnemyCode(string mapName, string difficulty)
        {
            switch (mapName)
            {
                case "prologue_map1":
                case "prologue_map2":
                    return "wolf*";
                case "chapter1_map1":
                case "chapter1_map2":
                case "chapter1_map3":
                    if (difficulty == "easy")
                        return "wolf*";
                    else
                        return "wolf**";
                default: return "easter-egg";
            }
        }
    }
}
