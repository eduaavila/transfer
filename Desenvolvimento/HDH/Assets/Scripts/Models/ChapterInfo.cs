﻿using HDH;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Models
{
    public class ChapterInfo
    {
        private readonly string chapterName;
        private readonly string startingMap;
        private readonly string startingDialog;
        private readonly int mapIndex;
        private readonly int dialogIndex;
        private readonly int mapMaxIndex;
        private readonly int dialogMaxIndex;
        private readonly int startingCursorDirection;
        private readonly int exploreDuration;
        private readonly int enigmaDuration;
        private string nextChapterType;
        private string nextChapterName;
        private readonly bool fromFile;
        private List<List<int>> revealedMap;
        private bool isFacingRight = false;
        private JObject chestInfo;
        public ChapterInfo()
        {
            fromFile = false;
            chapterName = "prologue";
            mapIndex = 0;
            dialogIndex = 0;
            mapMaxIndex = 2;
            dialogMaxIndex = 2;
            startingDialog = $"{chapterName}_{dialogIndex}";
            startingMap = $"{chapterName}_map{mapIndex}";
            exploreDuration = 30;
            enigmaDuration = 30;
            startingCursorDirection = 0;
            isFacingRight = true;
            revealedMap = new List<List<int>>();
            CrossSceneClass.Verbose("new ChapterInfo()", "prologue");
            string chestInfoTxt = Resources.Load<TextAsset>("JSON/chestInfo").text;
            CrossSceneClass.Verbose("chestInfo asset found", chestInfoTxt.Length.ToString() + " chars");
            JObject rawChestInfo = JsonConvert.DeserializeObject(chestInfoTxt) as JObject;
            CrossSceneClass.Verbose("chestInfo loaded");
            chestInfo = rawChestInfo["prologue"] as JObject;
        }
        public ChapterInfo(string chapterName, int mapIndex = 0, int dialogIndex = 1)
        {
            CrossSceneClass.Verbose("new ChapterInfo()", chapterName);
            fromFile = true;
            this.chapterName = chapterName.ToLower();
            this.mapIndex = mapIndex;
            this.dialogIndex = dialogIndex;
            revealedMap = new List<List<int>>();
            string chestInfoTxt = Resources.Load<TextAsset>("JSON/chestInfo").text;
            JObject rawChestInfo = JsonConvert.DeserializeObject(chestInfoTxt) as JObject;
            switch (this.chapterName)
            {
                case "prologue":
                    startingDialog = $"{this.chapterName}_{this.dialogIndex}";
                    startingMap = $"{this.chapterName}_map{this.mapIndex}";
                    mapMaxIndex = 2;
                    dialogMaxIndex = 2;
                    exploreDuration = 15;
                    enigmaDuration = 30;
                    startingCursorDirection = 0;
                    chestInfo = rawChestInfo[this.chapterName] as JObject;
                    break;
                case "chapter1":
                    startingDialog = $"{this.chapterName}_{this.dialogIndex}";
                    startingMap = $"{this.chapterName}_map{this.mapIndex}";
                    mapMaxIndex = 3;
                    dialogMaxIndex = 2;
                    exploreDuration = 30;
                    enigmaDuration = 30;
                    switch (this.mapIndex)
                    {
                        case 3: startingCursorDirection = 180; break;
                        default: startingCursorDirection = 0; break;

                    }
                    chestInfo = rawChestInfo[this.chapterName] as JObject;
                    break;
                case "ending":
                    startingDialog = "ending";
                    break;
                default:
                    CrossSceneClass.Log("Couldn't instantiate ChapterInfo, then ending.", chapterName);
                    this.chapterName = "ending";
                    startingDialog = "ending";
                    dialogMaxIndex = 1;
                    break;
            }
            chestInfo = (JsonConvert.DeserializeObject(chestInfoTxt) as JObject)[chapterName] as JObject;
            if (startingCursorDirection == 180)
                isFacingRight = false;
            else
                isFacingRight = true;
            
        }
        public List<List<int>> GetRevealedMap()
        {
            return revealedMap;
        }
        public void SetRevealedMap(List<List<int>> revealedMap)
        {
            this.revealedMap = revealedMap;
        }
        public static string GetNextChapterName(string chapterName)
        {
            string newChapter;
            switch(chapterName)
            {
                case "prologue": newChapter = "chapter1"; break;
                default: newChapter = "error"; break;
            }
            CrossSceneClass.Verbose("Going to next chapter", chapterName + " -> " + newChapter);
            return newChapter;
        }
        public bool IsLastDialogIndex()
        {
            return dialogIndex == dialogMaxIndex;
        }
        public bool IsLastMapIndex()
        {
            return mapIndex == mapMaxIndex;
        }
        public string GetChapterName()
        {
            return chapterName;
        }
        public string GetStartingMap()
        {
            return startingMap;
        }
        public string GetStartingDialog()
        {
            return startingDialog;
        }
        public int GetStartingCursorDirection()
        {
            return startingCursorDirection;
        }
        public int GetEnigmaDuration()
        {
            return enigmaDuration;
        }
        public int GetExploreDuration()
        {
            return exploreDuration;
        }
        public int GetMapIndex()
        {
            return mapIndex;
        }
        public int GetDialogIndex()
        {
            return dialogIndex;
        }
        public bool IsFromFile()
        {
            return fromFile;
        }
        public bool IsFacingRight()
        {
            return isFacingRight;
        }
        public void SetFacingRight(bool isFacingRight)
        {
            this.isFacingRight = isFacingRight;
        }
        public JObject GetChestInfo()
        {
            return chestInfo;
        }
        public void SetChestInfo(JObject chestInfo)
        {
            this.chestInfo = chestInfo;
        }
    }
}
