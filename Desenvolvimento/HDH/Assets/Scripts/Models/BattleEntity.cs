﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Models
{
    public class BattleEntity
    {
        protected float baseHp;
        protected float baseAtk;
        protected float baseDef;
        protected float baseSpeed;
        protected float hp;
        protected float sp;
        protected float atk;
        protected float def;
        protected float speed;
        public float GetBaseHp()
        {
            return baseHp;
        }
        public float GetBaseAtk()
        {
            return baseAtk;
        }
        public float GetBaseDef()
        {
            return baseDef;
        }
        public float GetBaseSpeed()
        {
            return baseSpeed;
        }
        public float GetHp()
        {
            return hp;
        }
        public float GetAtk()
        {
            return atk;
        }
        public float GetDef()
        {
            return def;
        }
        public float GetSpeed()
        {
            return speed;
        }
        public float GetSp()
        {
            return sp;
        }
        public void SetHp(float hp)
        {
            if (hp > 0)
                this.hp = hp;
            else
                this.hp = 0;
        }
        public void SetDef(float def)
        {
            if (def > 0)
                this.def = def;
            else
                this.def = 0;
        }
        public void SetAtk(float atk)
        {
            if (atk > 0)
                this.atk = atk;
            else
                this.atk = 0;
        }
        public void SetSpeed(float speed)
        {
            if (speed > 0)
                this.speed = speed;
            else
                this.speed = 0;
        }
        public void SetSp(float sp)
        {
            this.sp = sp;
        }
        public bool IsAlive()
        {
            return hp != 0;
        }
        public float GetHealthPercentage()
        {
            float raw = ((100f * hp) / baseHp) / 100f;
            float rounded = (float)Math.Round(raw, 2);
            return rounded;
        }
    }
}
