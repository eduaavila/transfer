﻿using Assets.Scripts.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace HDH
{
    public class ConquestBattleBehaviour : MonoBehaviour
    {
        private GameObject heroObj;
        private static List<GameObject> enemyObjs;
        private EnemyUnit enemyUnit;
        private Animator heroAnimator;
        private Hero hero;

        private GameObject atkModeBtn;
        private GameObject defModeBtn;
        private GameObject spdModeBtn;

        private Vector3 heroInitialPosition;
        private bool heroIsAttacking;
        private float heroAttckAnimTime;
        private float heroCrrntAttckTime;
        private float heroJmpSpd;
        private float enemyJmpSpd;

        private static int heroTarget = 0;
        private List<Vector3> enemyInitialPositions;

        private bool started = false;
        private bool finished = false;

        private static GameObject focusPrefab;
        private static GameObject focusRing;
        private enum BattleMode
        {
            Atk, 
            Def,
            Spd
        }
        private BattleMode mode = BattleMode.Atk;
        void Start()
        {
            if (!CrossSceneClass.IsLoaded())
            {
                CrossSceneClass.Log("Game was not loaded");
                SceneManager.LoadScene("MainMenuScene");
            }
            focusPrefab = Resources.Load<GameObject>(@"Prefabs/FocusRing");
            enemyObjs = new List<GameObject>();
            enemyInitialPositions = new List<Vector3>();
            heroJmpSpd = 10f;
            atkModeBtn = GameObject.Find("btnATK");
            defModeBtn = GameObject.Find("btnDEF");
            spdModeBtn = GameObject.Find("btnSPD");
            heroCrrntAttckTime = 0f;
            heroAttckAnimTime = 0.25f;
            heroIsAttacking = false;
            heroObj = GameObject.FindGameObjectsWithTag("Hero")[0];
            Transform dg = GameObject.Find("Dungeon").transform;
            heroAnimator = heroObj.GetComponent<Animator>();
            heroInitialPosition = heroObj.transform.localPosition;

            hero = CrossSceneClass.GetProfile().GetHero();

            CrossSceneClass.Log("Hero status set.", $"hp:{hero.GetHp()} atk:{hero.GetAtk()} def:{hero.GetDef()} spd:{hero.GetSpeed()}");
            enemyUnit = CrossSceneClass.GetEncounter();
            enemyJmpSpd = 10f;
            int i = 0;
            foreach(Enemy enemy in enemyUnit.GetEnemies())
            {
                GameObject obj = Instantiate(enemy.GetBattlePrefab(), dg);
                obj.name = i + obj.name.Replace("(Clone)", "");
                enemyObjs.Add(obj);
                CrossSceneClass.Log("NAME, HP, DEF, ATK, SPD", string.Join(", ", new string[]{ enemy.GetBattlePrefab().name, enemy.GetBaseHp().ToString(), enemy.GetBaseDef().ToString(), enemy.GetBaseAtk().ToString(), enemy.GetBaseSpeed().ToString() }));
                i++;
            }
            CrossSceneClass.Log("Enemies instantiated to battle.", enemyObjs.Count + " enemies.");
            Vector3 newPos;
            switch (enemyObjs.Count)
            {
                case 1:
                    CrossSceneClass.Log("Enemy instantiated", "1");
                    break;
                case 2:
                    CrossSceneClass.Log("Enemies instantiated", "2");
                    newPos = enemyObjs[0].transform.localPosition;
                    newPos.Set(newPos.x, newPos.y + 0.25f, newPos.z);
                    enemyObjs[0].transform.localPosition = newPos;
                    newPos = enemyObjs[1].transform.localPosition;
                    newPos.Set(newPos.x + 1f, newPos.y - 0.25f, newPos.z+1f);
                    enemyObjs[1].transform.localPosition = newPos;
                    enemyObjs[1].transform.GetComponent<SpriteRenderer>().sortingOrder = 4;
                    break;
                case 3:
                    CrossSceneClass.Log("Enemies instantiated", "3");
                    newPos = enemyObjs[0].transform.localPosition;
                    newPos.Set(newPos.x, newPos.y + 0.25f, newPos.z);
                    enemyObjs[0].transform.localPosition = newPos;
                    newPos = enemyObjs[1].transform.localPosition;
                    newPos.Set(newPos.x + 1f, newPos.y - 0.25f, newPos.z + 2f);
                    enemyObjs[1].transform.localPosition = newPos;
                    newPos = enemyObjs[2].transform.localPosition;
                    newPos.Set(newPos.x + 0.5f, newPos.y, newPos.z + 1f);
                    enemyObjs[2].transform.localPosition = newPos;
                    enemyObjs[1].transform.GetComponent<SpriteRenderer>().sortingOrder = 4;
                    enemyObjs[2].transform.GetComponent<SpriteRenderer>().sortingOrder = 6;
                    break;
                default:
                    Debug.LogError("No enemies instantiated");
                    Application.Quit();
                    break;
            }
            for (i = 0; i < enemyObjs.Count; i++)
            {
                enemyInitialPositions.Add(enemyObjs[i].transform.localPosition);
            }
            if(enemyUnit.IsBossFight())
            {
                AudioSource audioSrc = Camera.main.GetComponent<AudioSource>();
                audioSrc.clip = Resources.Load<AudioClip>(@"Sounds/BGM/incounter");
                audioSrc.Play();
            }
            ChangeMode(BattleMode.Atk);
            ChangeTarget(enemyObjs[0]);
            focusRing.GetComponent<Image>().color = Color.white;
            //Start of SP functions
            UpdateGameStatus();
            InvokeRepeating("UpdSPBars", 1.0f, 1.0f);
        }

        private void UpdateGameStatus()
        {
            if (!hero.IsAlive())
            {
                heroAnimator.SetBool("heroIsAlive", false);
                Invoke("GameOver", 1f);
                return;
            }
            List<Enemy> enemies = enemyUnit.GetEnemies();
            UpdateHeroBars();
            for (int i = 0; i < enemyObjs.Count; i++)
            {
                UpdateEnemyBars(enemies[i], i);
                if (!enemies[i].IsAlive())
                {
                    PlayEnemyDeathAnimation(enemies[i], enemyObjs[i]);
                }
            }
            if (enemyUnit.AllEnemiesDead())
            {
                finished = true;
                CrossSceneClass.GetProfile().GetHero().SetHp(hero.GetHp());
                CrossSceneClass.Log("Battle ended.", $"Hero HP: {CrossSceneClass.GetProfile().GetHero().GetHp()}");
                CrossSceneClass.GetActiveEnemies()[CrossSceneClass.GetActiveEnemies().IndexOf(CrossSceneClass.GetEncounter())].SetAlive(false);
                if(enemyUnit.IsBossFight())
                    Invoke("BossEnd", 2f);
                else
                    Invoke("ConquerMove", 2f);
            }
        }
        private void BossEnd()
        {
            CrossSceneClass.Cross();
        }
        private void PlayEnemyDeathAnimation(Enemy enemy, GameObject enemyObj)
        {
            if (!enemy.IsDead())
            {
                enemyObj.transform.GetComponent<Animator>().SetBool("enemyIsAlive", false);
            }
            enemy.SetAsDead(true);
        }

        private void UpdateHeroBars()
        {
            GameObject bars = heroObj.transform.Find("Bars").transform.Find("BarsPos").gameObject;
            Transform heroBar = bars.transform.Find("HP");
            Vector3 newScale = new Vector3(hero.GetHealthPercentage(), 1, 1);
            heroBar.transform.localScale = newScale;
            CrossSceneClass.Verbose("Updated hero bars", $"{hero.GetHeroType()}: HP {hero.GetHp()}/{hero.GetBaseHp()} ({hero.GetHealthPercentage() * 100}%); SP {hero.GetSp() * 100}%");
            if (!started)
            {
                started = true;
                hero.SetSp(0);
            }
            heroBar = bars.transform.Find("SP");
            newScale = new Vector3(hero.GetSp(), 1, 1);
            heroBar.transform.localScale = newScale;
        }
        private void UpdateEnemyBars(Enemy enemy, int i)
        {
            GameObject bars = enemyObjs[i].transform.Find("Bars").Find("BarsPos").gameObject;
            Transform enemyBar = bars.transform.Find("HP");
            Vector3 newScale = new Vector3(enemy.GetHealthPercentage(), 1, 1);
            enemyBar.transform.localScale = newScale;
            enemyBar = bars.transform.Find("SP");
            newScale = new Vector3(enemy.GetSp(), 1, 1);
            enemyBar.transform.localScale = newScale;
            CrossSceneClass.Verbose("Updated enemy bars", $"Unit[{i}] {enemy.GetName()}: HP {enemy.GetHp()}/{enemy.GetBaseHp()} ({enemy.GetHealthPercentage() * 100}%); SP {enemy.GetSp() * 100}%");
        }

        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100))
                {
                    Debug.Log(hit.transform.gameObject.name);
                }
            }
            //gerenciamento de anim de morte de lobos
            for (int i = 0; i < enemyObjs.Count; i++)
            {
                if (enemyObjs[i].GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Die"))
                {
                    enemyObjs[i].GetComponent<Animator>().SetBool("Stop", true);
                }
            }
            //gerenciamento de animacao e pulo dos lobos
            for (int i = 0; i < enemyObjs.Count; i++)
            {
                if (enemyObjs[i].GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                {
                    enemyObjs[i].GetComponent<Animator>().SetBool("enemyIsAttacking", false);
                    if(enemyObjs[i].transform.localPosition.x > heroObj.transform.localPosition.x + 0.5f)
                    {
                        enemyObjs[i].transform.Translate((enemyJmpSpd + 100 * Time.deltaTime), 0f, 0f);
                    }
                }
                else
                {
                    enemyObjs[i].transform.localPosition = enemyInitialPositions[i];
                }
            }
            if (heroIsAttacking && heroCrrntAttckTime <= heroAttckAnimTime)
            {
                heroCrrntAttckTime += Time.deltaTime;
                heroAnimator.SetBool("heroIsAttacking", heroIsAttacking);
                //Movimentacao durante atk anim
                if(heroObj.transform.localPosition.x < enemyObjs[heroTarget].transform.localPosition.x - 0.5f)
                {
                    heroJmpSpd += 100*Time.deltaTime;
                    heroObj.transform.Translate(-1*heroJmpSpd, 0f ,0f);
                }
                else
                {
                    heroJmpSpd = 2f;
                }
            }
            else
            {
                //Movimentacao para o ponto inicial apos atk anim
                if (heroObj.transform.localPosition.x > heroInitialPosition.x)
                {
                    heroJmpSpd += Time.deltaTime;
                    heroObj.transform.Translate(heroJmpSpd, 0f, 0f);
                }
                else
                {
                    heroJmpSpd = 2f;
                }
                heroCrrntAttckTime = 0;
                heroAnimator.SetBool("heroIsAttacking", heroIsAttacking);
            }
            if (heroIsAttacking && heroCrrntAttckTime >= heroAttckAnimTime)
            {
                heroIsAttacking = false;
                heroAnimator.SetBool("heroIsAttacking", heroIsAttacking);
            }
        }

        private void HeroAttack()
        {
            heroIsAttacking = true;
            Enemy enemy = enemyUnit.GetEnemies()[heroTarget];
            CrossSceneClass.Log("Hero is attacking", $"target:{enemy.GetName()} HP({enemy.GetHp()}/{enemy.GetBaseHp()})");
            float heroAtk = hero.GetAtk() + (mode == BattleMode.Atk ? 5 : 0);
            float dmg = CalcDamage(heroAtk, enemy.GetDef());
            float newHp = enemy.GetHp() - dmg;
            enemy.SetHp(newHp);
            enemyUnit.GetEnemies()[heroTarget] = enemy;
            CrossSceneClass.Log($"{enemy.GetName()} took {dmg} damage", $"target:{enemy.GetName()} HP({enemy.GetHp()}/{enemy.GetBaseHp()})");
        }

        private void ConquerMove()
        {
            SceneManager.LoadScene("ConquerMoveScene");
        }
        private void GameOver()
        {
            SceneManager.LoadScene("GameOverScene");
        }

        private bool AllEnemyDead()
        {
            return !enemyUnit.GetEnemies().Exists(x => x.IsAlive());
        }

        private void UpdSPBars() // Invoked every 1s
        {
            int enemyIndex = 0;
            if (finished) return;
            float newVal = hero.GetSp() + (hero.GetSpeed() / 100) + (mode == BattleMode.Spd ? 0.1f : 0);
            hero.SetSp(newVal);
            if(hero.GetSp() >= 1)
            {
                HeroAttack();
                hero.SetSp(hero.GetSp() % 1);
            }
            // TODO Enemy new SP bars
            foreach (Enemy enemy in enemyUnit.GetEnemies())
            {
                if (enemy.IsAlive() == false)
                    continue;

                newVal = enemy.GetSp() + (enemy.GetSpeed() / 100);
                enemy.SetSp(newVal);
                if(enemy.GetSp() >= 1)
                {
                    EnemyAttack(enemy, enemyObjs[enemyIndex], enemyIndex);
                    enemy.SetSp((enemy.GetSp() % 1));
                }
                enemyIndex++;
            }
            UpdateGameStatus();
        }
        private void EnemyAttack(Enemy enemySrc, GameObject enemyGameObj, int enemyIndex)
        {
            enemyGameObj.transform.GetComponent<Animator>().SetBool("enemyIsAttacking", true);
            CrossSceneClass.Log(enemySrc.GetName() + " attacks", $"Hero HP({hero.GetHp()}/{hero.GetBaseHp()})");
            float enemyAtk = enemySrc.GetAtk();
            float heroDef = hero.GetDef() + (mode == BattleMode.Def ? 6 : 0);
            float dmg = CalcDamage(enemyAtk, heroDef);
            float newHp = hero.GetHp() - dmg;
            hero.SetHp(newHp);
            CrossSceneClass.Log($"Hero took {dmg} damage", $"Hero HP({hero.GetHp()}/{hero.GetBaseHp()})");
        }
        private float CalcDamage(float attackerAtk, float defenderDef)
        {
            CrossSceneClass.Verbose("CalcDamage", $"atk:{attackerAtk} def:{defenderDef}");
            float damage = attackerAtk - defenderDef;
            return (damage > 0 ? damage : 0);
        }

        private static bool LoyaltyCheck()
        {
            //Substituir 100 por completude do mapa
            float completionRate = float.Parse((CrossSceneClass.GetCurrentMap().GetCompletionRate() * 100).ToString());
            CrossSceneClass.GetProfile().GetHero().UpdateLoyalty(completionRate);
            bool isLoyal = CrossSceneClass.GetProfile().GetHero().IsLoyal();
            CrossSceneClass.Log("Loyalty Check", $"isLoyal:{isLoyal.ToString()}");
            return isLoyal;
        }

        private void DisableModeBtns()
        {
            atkModeBtn.GetComponent<Button>().interactable = false;
            defModeBtn.GetComponent<Button>().interactable = false;
            spdModeBtn.GetComponent<Button>().interactable = false;
            atkModeBtn.GetComponent<Image>().color = Color.red;
            defModeBtn.GetComponent<Image>().color = Color.red;
            spdModeBtn.GetComponent<Image>().color = Color.red;
        }

        private void EnableModeBtns()
        {
            atkModeBtn.GetComponent<Button>().interactable = true;
            defModeBtn.GetComponent<Button>().interactable = true;
            spdModeBtn.GetComponent<Button>().interactable = true;
            atkModeBtn.GetComponent<Image>().color = Color.white;
            defModeBtn.GetComponent<Image>().color = Color.white;
            spdModeBtn.GetComponent<Image>().color = Color.white;
            switch (mode)
            {
                case BattleMode.Atk: atkModeBtn.GetComponent<Button>().interactable = false; break;
                case BattleMode.Def: defModeBtn.GetComponent<Button>().interactable = false; break;
                case BattleMode.Spd: spdModeBtn.GetComponent<Button>().interactable = false; break;
            }
        }

        private void RandomMode(BattleMode desiredMode)
        {
            int rand = Random.Range(0, 2);
            switch (desiredMode)
            {
                case BattleMode.Atk:
                    if (rand == 0)
                        ChangeMode(BattleMode.Def);
                    else 
                        ChangeMode(BattleMode.Spd);
                    break;
                case BattleMode.Def:
                    if (rand == 0)
                        ChangeMode(BattleMode.Atk);
                    else 
                        ChangeMode(BattleMode.Spd);
                    break;
                case BattleMode.Spd:
                    if (rand == 0)
                        ChangeMode(BattleMode.Def);
                    else 
                        ChangeMode(BattleMode.Atk);
                break;
                default: CrossSceneClass.Log("Unhandled BattleMode", mode.ToString()); break;
            }
        }

        private void ChangeMode(BattleMode newMode)
        {
            switch (newMode)
            {
                case BattleMode.Atk:
                    atkModeBtn.GetComponent<Button>().interactable = false;
                    defModeBtn.GetComponent<Button>().interactable = true;
                    spdModeBtn.GetComponent<Button>().interactable = true;
                    break;
                case BattleMode.Def:
                    defModeBtn.GetComponent<Button>().interactable = false;
                    atkModeBtn.GetComponent<Button>().interactable = true;
                    spdModeBtn.GetComponent<Button>().interactable = true;
                    break;
                case BattleMode.Spd:
                    spdModeBtn.GetComponent<Button>().interactable = false;
                    atkModeBtn.GetComponent<Button>().interactable = true;
                    defModeBtn.GetComponent<Button>().interactable = true;
                    break;
                default: CrossSceneClass.Log("Unhandled BattleMode", newMode.ToString()); break;
            }
            CrossSceneClass.Log("Changed battle mode", newMode.ToString());
            mode = newMode;
        }
        public void AtkMode()
        {
            CrossSceneClass.Log("Trying to change battle mode", "Atk");
            TryChangeBattleMode(BattleMode.Atk);
        }
        public void DefMode()
        {
            CrossSceneClass.Log("Trying to change battle mode", "Def");
            TryChangeBattleMode(BattleMode.Def);
        }
        public void SpdMode()
        {
            CrossSceneClass.Log("Trying to change battle mode", "Spd");
            TryChangeBattleMode(BattleMode.Spd);
        }
        private void TryChangeBattleMode(BattleMode desiredMode)
        {
            if (LoyaltyCheck())
            {
                ChangeMode(desiredMode);
            }
            else
            {
                RandomMode(desiredMode);
                DisableModeBtns();
                Invoke("EnableModeBtns", 3f);
            }
        }
        public void SetHeroTarget(int heroTarget)
        {
            ConquestBattleBehaviour.heroTarget = heroTarget;
        }
        public static void ChangeTarget(GameObject self)
        {
            Color color;
            List<Enemy> enemies = CrossSceneClass.GetEncounter().GetEnemies();
            int target = int.Parse(self.name[0].ToString());
            CrossSceneClass.Verbose("Targetting enemy", target.ToString());
            color = Color.red;
            if (!LoyaltyCheck())
            {
                int random = Random.Range(00, enemies.Count);
                target = random;
                self = enemyObjs[random];
            }
            Destroy(GameObject.Find("FocusRing"));
            focusRing = Instantiate(focusPrefab, GameObject.Find("Dungeon").transform);
            focusRing.name = focusRing.name.Replace("(Clone)", "");
            Vector3 pos = self.transform.localPosition;
            if (self.name.EndsWith("_boss"))
                pos.Set(pos.x, pos.y - 0.5f, pos.z);
            focusRing.transform.localPosition = pos;
            focusRing.GetComponent<Image>().color = color;
            heroTarget = target;
            CrossSceneClass.Log($"Target changed to target {target}");
        }
        public static void ReenableFocus()
        {
            if(EnemyBehaviour.IsAllowingFocusChange())
                focusRing.GetComponent<Image>().color = Color.white;
        }
    }
}