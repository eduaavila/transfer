﻿using Assets.Scripts.Models;
using UnityEngine;

namespace HDH
{
    public class MouseDragY : MouseDrag
    {
        private void Start()
        {
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
        }
    }
}