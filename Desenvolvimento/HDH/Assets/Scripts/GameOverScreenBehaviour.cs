﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace HDH
{
    public class GameOverScreenBehaviour : MonoBehaviour
    {
        private string selectedObjectname;

        // Start is called before the first frame update
        void Start()
        {
            CrossSceneClass.Log("Game Over.");
        }

        public void Selected(GameObject sender)
        {
            selectedObjectname = sender.name;
        }
        private void Update()
        {
            if (selectedObjectname != "")
            {
                switch (selectedObjectname)
                {
                    case "btnLoadMainMenu":
                        SceneManager.LoadScene("MainMenuScene");
                        break;
                    case "btnLoadGame":
                        CrossSceneClass.LoadFile();
                        break;
                    case "btnExitGame":
                        Application.Quit();
                        break;
                }
                selectedObjectname = "";
            }

        }
    }
}