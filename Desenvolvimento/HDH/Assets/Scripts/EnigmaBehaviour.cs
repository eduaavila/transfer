﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace HDH
{
    public class EnigmaBehaviour : MonoBehaviour
    {
        public GameObject board;
        public Material borderMat;
        public Material boardBMat;
        public Material keyMat;
        public Material lockMat;

        private readonly Vector3[,] grid = new Vector3[10,10];
        private float cellSize;

        private static bool useTimeLimit = true;

        private float timeLimit;
        public GameObject counter;
        void Start()
        {
            if (!CrossSceneClass.IsLoaded())
            {
                Debug.Log($"File is not loaded");
                SceneManager.LoadScene("MainMenuScene");
            }
            timeLimit = CrossSceneClass.GetProfile().GetChapterInfo().GetEnigmaDuration();
            counter.transform.Find("seconds").GetComponent<Text>().text = Convert.ToInt32(timeLimit).ToString();
            if (!useTimeLimit)
            {
                counter.SetActive(false);
            }

            InitBoard(6);
            int puzzleId = UnityEngine.Random.Range(00, 16);
            CrossSceneClass.Log("Enigma puzzle loaded", $"Id:{puzzleId}");
            GeneratePuzzle(puzzleId);
        }

        void Update()
        {
            
        }

        private void CountTime()
        {
            timeLimit -= 1;
            if (timeLimit > 0)
            {
                counter.transform.Find("seconds").GetComponent<Text>().text = Convert.ToInt32(timeLimit).ToString();
            }
            else
            {
                counter.transform.Find("seconds").GetComponent<Text>().text = "0";
                SceneManager.LoadScene("ConquerMoveScene");
            }
        }

        private void GeneratePuzzle(int pattern)
        {
            switch (pattern)
            {
                case 00:
                    GenerateBox(0, 2, 1, 2, true);
                    GenerateBox(2, 1, 2, 2, false);
                    GenerateBox(3, 0, 4, 0, false);
                    GenerateBox(4, 1, 4, 2, false);
                    GenerateBox(5, 1, 5, 2, false);
                    GenerateBox(4, 3, 5, 3, false);
                    break;
                case 01:
                    GenerateBox(0, 2, 1, 2, true);
                    GenerateBox(1, 4, 1, 5, false);
                    GenerateBox(3, 4, 4, 4, false);
                    GenerateBox(2, 1, 2, 2, false);
                    GenerateBox(4, 0, 5, 0, false);
                    GenerateBox(3, 0, 3, 2, false);
                    GenerateBox(2, 5, 3, 5, false);
                    GenerateBox(5, 3, 5, 4, false);
                    GenerateBox(3, 3, 4, 3, false);
                    GenerateBox(4, 5, 5, 5, false);
                    break;
                case 02:
                    GenerateBox(0, 2, 1, 2, true);
                    GenerateBox(4, 2, 4, 3, false);
                    GenerateBox(4, 4, 5, 4, false);
                    GenerateBox(3, 1, 3, 2, false);
                    GenerateBox(4, 5, 5, 5, false);
                    GenerateBox(5, 0, 5, 2, false);
                    GenerateBox(2, 0, 3, 0, false);
                    GenerateBox(3, 3, 3, 4, false);
                    GenerateBox(0, 0, 1, 0, false);
                    break;
                case 03:
                    GenerateBox(0, 2, 1, 2, true);
                    GenerateBox(5, 1, 5, 3, false);
                    GenerateBox(2, 3, 3, 3, false);
                    GenerateBox(3, 1, 3, 2, false);
                    GenerateBox(2, 0, 3, 0, false);
                    GenerateBox(4, 1, 4, 3, false);
                    GenerateBox(4, 5, 5, 5, false);
                    GenerateBox(0, 3, 0, 5, false);
                    GenerateBox(4, 4, 5, 4, false);
                    GenerateBox(1, 5, 2, 5, false);
                    break;
                case 04:
                    GenerateBox(0, 2, 1, 2, true);
                    GenerateBox(4, 1, 4, 3, false);
                    GenerateBox(3, 5, 4, 5, false);
                    GenerateBox(5, 1, 5, 2, false);
                    GenerateBox(1, 3, 2, 3, false);
                    GenerateBox(3, 1, 3, 2, false);
                    GenerateBox(2, 4, 2, 5, false);
                    GenerateBox(0, 3, 0, 5, false);
                    GenerateBox(2, 1, 2, 2, false);
                    break;
                case 05:
                    //Facil
                    GenerateBox(0, 0, 0, 1, false);
                    GenerateBox(0, 2, 1, 2, true);
                    GenerateBox(0, 4, 0, 5, false);
                    GenerateBox(1, 3, 1, 4, false);
                    GenerateBox(2, 1, 2, 2, false);
                    GenerateBox(2, 3, 3, 3, false);
                    GenerateBox(2, 4, 2, 5, false);
                    GenerateBox(4, 1, 4, 2, false);
                    GenerateBox(5, 0, 5, 2, false);
                    GenerateBox(4, 4, 5, 4, false);
                    GenerateBox(3, 5, 5, 5, false);
                    break;
                case 06:
                    //Medio
                    GenerateBox(0, 1, 1, 1, false);
                    GenerateBox(0, 2, 1, 2, true);
                    GenerateBox(0, 4, 0, 5, false);
                    GenerateBox(3, 0, 3, 1, false);
                    GenerateBox(2, 2, 2, 4, false);
                    GenerateBox(1, 5, 3, 5, false);
                    GenerateBox(5, 0, 5, 2, false);
                    GenerateBox(3, 3, 5, 3, false);
                    GenerateBox(4, 4, 4, 5, false);
                    break;
                case 07:
                    //Medio
                    GenerateBox(1, 0, 3, 0, false);
                    GenerateBox(0, 1, 2, 1, false);
                    GenerateBox(3, 1, 3, 3, false);
                    GenerateBox(0, 2, 1, 2, true);
                    GenerateBox(1, 3, 2, 3, false);
                    GenerateBox(0, 4, 0, 5, false);
                    GenerateBox(2, 4, 2, 5, false);
                    GenerateBox(3, 4, 4, 4, false);
                    GenerateBox(3, 5, 5, 5, false);
                    break;
                case 08:
                    //Medio
                    GenerateBox(2, 0, 4, 0, false);
                    GenerateBox(5, 0, 5, 1, false);
                    GenerateBox(0, 1, 1, 1, false);
                    GenerateBox(0, 2, 1, 2, true);
                    GenerateBox(2, 1, 2, 2, false);
                    GenerateBox(4, 1, 4, 2, false);
                    GenerateBox(5, 2, 5, 4, false);
                    GenerateBox(0, 3, 0, 4, false);
                    GenerateBox(2, 3, 3, 3, false);
                    GenerateBox(2, 4, 4, 4, false);
                    GenerateBox(4, 5, 5, 5, false);
                    break;
                case 09:
                    //Medio
                    GenerateBox(0, 0, 1, 0, false);
                    GenerateBox(2, 0, 4, 0, false);
                    GenerateBox(1, 1, 3, 1, false);
                    GenerateBox(4, 1, 4, 3, false);
                    GenerateBox(5, 1, 5, 2, false);
                    GenerateBox(0, 2, 1, 2, true);
                    GenerateBox(0, 3, 1, 3, false);
                    GenerateBox(2, 2, 2, 3, false);
                    GenerateBox(5, 3, 5, 4, false);
                    GenerateBox(0, 4, 0, 5, false);
                    GenerateBox(1, 4, 3, 4, false);
                    GenerateBox(3, 5, 5, 5, false);
                    break;
                case 10:
                    //Avançado
                    GenerateBox(0, 0, 1, 0, false);
                    GenerateBox(2, 0, 4, 0, false);
                    GenerateBox(1, 1, 3, 1, false);
                    GenerateBox(4, 1, 4, 3, false);
                    GenerateBox(5, 1, 5, 2, false);
                    GenerateBox(5, 3, 5, 4, false);
                    GenerateBox(0, 2, 1, 2, true);
                    GenerateBox(0, 3, 1, 3, false);
                    GenerateBox(2, 2, 2, 3, false);
                    GenerateBox(0, 4, 0, 5, false);
                    GenerateBox(1, 4, 3, 4, false);
                    GenerateBox(3, 5, 5, 5, false);
                    break;
                case 11:
                    //Avançado
                    GenerateBox(0, 1, 1, 1, false);
                    GenerateBox(0, 2, 1, 2, true);
                    GenerateBox(0, 4, 0, 5, false);
                    GenerateBox(2, 0, 3, 0, false);
                    GenerateBox(2, 1, 2, 3, false);
                    GenerateBox(1, 4, 2, 4, false);
                    GenerateBox(2, 5, 4, 5, false);
                    GenerateBox(3, 3, 3, 4, false);
                    GenerateBox(5, 0, 5, 2, false);
                    break;
                case 12:
                    //Avançado
                    GenerateBox(2, 0, 3, 0, false);
                    GenerateBox(4, 0, 4, 1, false);
                    GenerateBox(1, 1, 2, 1, false);
                    GenerateBox(3, 1, 3, 2, false);
                    GenerateBox(0, 2, 1, 2, true);
                    GenerateBox(2, 2, 2, 4, false);
                    GenerateBox(0, 3, 0, 4, false);
                    GenerateBox(3, 3, 4, 3, false);
                    GenerateBox(4, 4, 4, 5, false);
                    GenerateBox(0, 5, 2, 5, false);
                    break;
                case 13:
                    //Dificil
                    GenerateBox(0, 0, 0, 1, false);
                    GenerateBox(1, 0, 2, 0, false);
                    GenerateBox(3, 0, 4, 0, false);
                    GenerateBox(5, 0, 5, 1, false);
                    GenerateBox(3, 1, 3, 3, false);
                    GenerateBox(0, 2, 1, 2, true);
                    GenerateBox(1, 3, 2, 3, false);
                    GenerateBox(4, 3, 5, 3, false);
                    GenerateBox(0, 4, 1, 4, false);
                    GenerateBox(2, 4, 3, 4, false);
                    GenerateBox(4, 4, 4, 5, false);
                    break;
                case 14:
                    //Dificil
                    GenerateBox(0, 1, 1, 1, false);
                    GenerateBox(0, 2, 1, 2, true);
                    GenerateBox(2, 0, 2, 2, false);
                    GenerateBox(3, 0, 5, 0, false);
                    GenerateBox(3, 1, 4, 1, false);
                    GenerateBox(5, 1, 5, 2, false);
                    GenerateBox(3, 2, 3, 4, false);
                    GenerateBox(0, 3, 0, 4, false);
                    GenerateBox(1, 3, 2, 3, false);
                    GenerateBox(1, 4, 2, 4, false);
                    GenerateBox(4, 3, 4, 4, false);
                    GenerateBox(0, 5, 1, 5, false);
                    break;
                case 15:
                    //Especialista
                    GenerateBox(0, 0, 0, 1, false);
                    GenerateBox(1, 0, 2, 0, false);
                    GenerateBox(3, 0, 4, 0, false);
                    GenerateBox(1, 1, 3, 1, false);
                    GenerateBox(4, 1, 4, 3, false);
                    GenerateBox(0, 2, 1, 2, true);
                    GenerateBox(0, 3, 1, 3, false);
                    GenerateBox(3, 2, 3, 3, false);
                    GenerateBox(0, 4, 0, 5, false);
                    GenerateBox(1, 4, 1, 5, false);
                    GenerateBox(2, 5, 3, 5, false);
                    GenerateBox(4, 5, 5, 5, false);
                    break;
            }
        }

        private void InitBoard(int puzzleSize)
        {
            if (useTimeLimit)
                InvokeRepeating("CountTime", 1f, 1f);

            cellSize = 1f / puzzleSize;


            for (int i = 0; i < puzzleSize; i++)
            {
                for (int j = 0; j < puzzleSize; j++)
                {
                    grid[i, j] = new Vector3(-0.5f+((i+0.5f)*cellSize), 0.5f - ((j + 0.5f) * cellSize), -3f);

                    GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    cube.transform.SetParent(board.transform);

                    cube.GetComponent<Renderer>().material = boardBMat;

                    cube.transform.localScale = new Vector3(cellSize * 0.9f, cellSize * 0.9f, 0.1f);
                    cube.transform.localPosition = new Vector3(grid[i, j].x, grid[i, j].y, -2f);
                }
            }
            GameObject borderN = GameObject.CreatePrimitive(PrimitiveType.Cube);
            GameObject borderS = GameObject.CreatePrimitive(PrimitiveType.Cube);
            GameObject goal = new GameObject("Goal");

            borderN.transform.SetParent(board.transform);
            borderS.transform.SetParent(board.transform);
            goal.transform.SetParent(board.transform);

            borderN.GetComponent<Renderer>().material = borderMat;
            borderS.GetComponent<Renderer>().material = borderMat;

            borderN.transform.localScale = new Vector3(0.04f, (2f / 6f), 1f);
            borderS.transform.localScale = new Vector3(0.04f, (3f / 6f), 1f);
            goal.transform.localScale = new Vector3(0.1f, 0.1f, 1f);

            borderN.transform.localPosition = new Vector3(-0.48f + (6f * cellSize), 0.5f - (1f * cellSize), -2f);
            borderS.transform.localPosition = new Vector3(-0.48f + (6f * cellSize), 0.5f - (4.5f * cellSize), -2f);
            goal.transform.localPosition = new Vector3(-0.40f + (6f * cellSize), 0.5f - (2.5f * cellSize), -2f);

            DestroyImmediate(borderN.GetComponent<BoxCollider>());
            DestroyImmediate(borderS.GetComponent<BoxCollider>());

            borderN.AddComponent<BoxCollider2D>();
            borderS.AddComponent<BoxCollider2D>();

            BoxCollider2D collider = goal.AddComponent<BoxCollider2D>();
            goal.AddComponent<GoalCollider>();
            collider.isTrigger = true;
        }

        private void GenerateBox(int x1, int y1, int x2, int y2, bool key)
        {
            Vector3 boxPos = Vector3.Lerp(grid[x1, y1], grid[x2, y2], 0.5f);

            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.SetParent(board.transform);
            if(key) cube.GetComponent<Renderer>().material = keyMat;
            else cube.GetComponent<Renderer>().material = lockMat;

            DestroyImmediate(cube.GetComponent<BoxCollider>());
            cube.AddComponent<BoxCollider2D>();
            Rigidbody2D body = cube.AddComponent<Rigidbody2D>();
            body.gravityScale = 0;
            body.angularDrag = 0;
            body.angularVelocity = 0;

            if (x1 == x2){
                int size = Mathf.Abs(y1 - y2);
                size++;
                cube.transform.localScale = new Vector3(cellSize * 0.9f, cellSize * 0.9f * size, 0.1f);
                cube.AddComponent<MouseDragY>();
            } else {
                int size = Mathf.Abs(x1 - x2);
                size++;
                cube.transform.localScale = new Vector3(cellSize * 0.9f * size, cellSize * 0.9f, 0.1f);
                cube.AddComponent<MouseDragX>();
            }
            cube.transform.localPosition = boxPos;
        }

        public static void EnableTimeLimit(bool enable)
        {
            useTimeLimit = enable;
        }
    }
}