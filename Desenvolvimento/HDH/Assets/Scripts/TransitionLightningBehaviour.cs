﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace HDH
{
    public class TransitionLightningBehaviour : MonoBehaviour
    {
        private float duration;
        private SpriteRenderer sprite;
        private float opacity;
        private string sceneToLoad;

        // Start is called before the first frame update
        void Start()
        {
            duration = 0f;
            sprite = GetComponent<SpriteRenderer>();
            opacity = 0.8f;
            DefineAlpha(opacity);
        }

        // Update is called once per frame
        void Update()
        {
            DefineAlpha(opacity);
            if (transform.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !transform.GetComponent<Animator>().IsInTransition(0))
            {
                if (sceneToLoad != "" && sceneToLoad != null)
                {
                    Debug.Log("LightningTransition ended, loading next scene!");
                    //loading da próxima cena aqui ex: SceneManager.LoadScene("sceneToLoad");
                }
                else
                {
                    Debug.Log("LightningTransition ended, but there is no scene to load, destroying itself!");
                    DestroyImmediate(this.gameObject);
                }
            }
        }
        public void SetDuration(float duration)
        {
            Debug.Log("Duration Altered");
            this.duration = duration; //Guardando o valor para futuro e possível uso, apesar de no momento ser desnecessario
            this.transform.GetComponent<Animator>().SetFloat("multiplier", 1f/this.duration);
        }
        public void SetInitialOpacity(float opacity)
        {
            Debug.Log("Opacity Altered");
            this.opacity = opacity;
        }
        private void DefineAlpha(float factor)
        {
            sprite = GetComponent<SpriteRenderer>();
            sprite.color = new Color(1f, 1f, 1f, factor);
        }
        public void DefineSceneToLoad(string sceneToLoad)
        {
            this.sceneToLoad = sceneToLoad;
        }
    }

}