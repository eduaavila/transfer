﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace HDH
{
    public class KnightBehavior : MonoBehaviour
    {
        private bool isMoving;
        private float currentAnimationTime;
        private SpriteRenderer sprite;
        private Animator animator;
        private float maxAnimationTime;
        private bool isfacingRight;

        private ButtonController btnc;


        // Start is called before the first frame update
        void Start()
        {
            btnc = EventSystem.current.GetComponent<ButtonController>();
            currentAnimationTime = 0;
            maxAnimationTime = 0.8f;
            isMoving = false;
            sprite = GetComponent<SpriteRenderer>();
            animator = GetComponent<Animator>();
            isfacingRight = true;
        }

        public bool GetIsMoving()
        {
            return isMoving;
        }

        public string GetFacingDirection()
        {
            if (isfacingRight)
            {
                return "facingRight";
            }
            else
                return "facingLeft";
        }
        public void SetIsMoving(bool isMoving)
        {
            this.isMoving = isMoving;
        }

        // Update is called once per frame
        void Update()
        {
            if (isMoving && currentAnimationTime >= maxAnimationTime)
            {
                isMoving = false;
                animator.SetBool("isMoving", false);
            }

            if (isMoving && currentAnimationTime <= maxAnimationTime)
            {
                currentAnimationTime += Time.deltaTime;
                animator.SetBool("isMoving", true);
            }
            else
            {
                currentAnimationTime = 0;
                animator.SetBool("isMoving", false);
            }
        }
        public void FlipKnight()
        {
            if (sprite != null)
            {
                if (CrossSceneClass.GetProfile().GetChapterInfo().IsFacingRight())
                {
                    sprite.flipX = false;
                    isfacingRight = true;
                }
                else
                {
                    sprite.flipX = true;
                    isfacingRight = false;
                }
            }
        }

        public bool IsMoving()
        {
            return isMoving;
        }
    }
}