﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace HDH
{
    public class SimpleTutorialBehaviour : MonoBehaviour
    {
        // Start is called before the first frame update
        private GameObject tutorialBg;
        private Image tutorialBgImage;

        public Sprite Exp1;
        public Sprite Exp2;
        public Sprite Exp3;
        public Sprite Exp4;
        public Sprite Exp5;
        public Sprite Mov1;
        public Sprite MovR;
        public Sprite Enm1;
        public Sprite Bat1;
        public Sprite Bat2;
        public Sprite Bat3;
        public Sprite Bat4;
        public Sprite BatR;

        //public Sprite explore;
        //public Sprite move;
        //public Sprite moveIgnored;
        //public Sprite moveDiscover;
        //public Sprite combat;
        //public Sprite combatIgnored;
        //public Sprite enigma;

        void Start()//.GetComponent<UnityEngine.UI.Image>().sprite
        {
            tutorialBg = transform.Find("TutorialBg").gameObject;
            tutorialBgImage = tutorialBg.GetComponent<UnityEngine.UI.Image>();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown("space"))
            {
                ButtonNextClicked();
            }
        }
        public void ButtonNextClicked()
        {
            switch (tutorialBgImage.sprite.name)
            {
                case "Exp-1": tutorialBgImage.sprite = Exp2; break;
                case "Exp-2": tutorialBgImage.sprite = Exp3; break;
                case "Exp-3": tutorialBgImage.sprite = Exp4; break;
                case "Exp-4": tutorialBgImage.sprite = Exp5; break;
                case "Exp-5": tutorialBgImage.sprite = Mov1; break;
                case "Mov-1": tutorialBgImage.sprite = MovR; break;
                case "Mov-R": tutorialBgImage.sprite = Enm1; break;
                case "Enm-1": tutorialBgImage.sprite = Bat1; break;
                case "Bat-1": tutorialBgImage.sprite = Bat2; break;
                case "Bat-2": tutorialBgImage.sprite = Bat3; break;
                case "Bat-3": tutorialBgImage.sprite = Bat4; break;
                case "Bat-4": tutorialBgImage.sprite = BatR; break;
                //case "Explore": tutorialBgImage.sprite = move; break;
                //case "Move": tutorialBgImage.sprite = moveIgnored; break;
                //case "MoveIgnored": tutorialBgImage.sprite = moveDiscover; break;
                //case "MoveDiscover": tutorialBgImage.sprite = combat; break;
                //case "Combat": tutorialBgImage.sprite = combatIgnored; break;
                //case "CombatIgnored": tutorialBgImage.sprite = enigma; break;
                case "Bat-R":
                    CrossSceneClass.SetCreate(true);
                    CrossSceneClass.LoadFile();
                    break;
                default:
                    CrossSceneClass.SetCreate(true);
                    CrossSceneClass.LoadFile();
                    break;
            }
        }
        public void skipTutorial()
        {
            CrossSceneClass.SetCreate(true);
            CrossSceneClass.LoadFile();
        }
    }

}