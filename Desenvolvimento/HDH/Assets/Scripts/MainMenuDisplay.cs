﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace HDH
{
    public class MainMenuDisplay : MonoBehaviour
    {
        public Slider sldVol;
        private string selectedObjectname;
        private int easterEgg;

        private void Start()
        {
            CrossSceneClass.Log("Game started.");
        }
        public void AdvertisingEasterEgg()
        {
            easterEgg++;
            if (easterEgg == 5)
            {
                var texts = FindObjectsOfType<Text>();
                foreach(Text t in texts)
                    t.text = "Curta Los Patetas";
                CrossSceneClass.Log("CURTA IMEDIATAMENTE: ", "Los Patetas");
            }
        }
        public void Selected(GameObject sender)
        {
            selectedObjectname = sender.name;
        }
        private void Update() {
            if (selectedObjectname != "")
            {
                switch (selectedObjectname)
                {
                    case "btnStartGame":
                        SceneManager.LoadScene("TutorialSimple");
                        break;
                    case "btnLoadGame":
                        CrossSceneClass.LoadFile();
                        break;
                    case "btnExitGame":
                        Application.Quit();
                        break;
                }
                selectedObjectname = "";
            }
        
        }
    }
}