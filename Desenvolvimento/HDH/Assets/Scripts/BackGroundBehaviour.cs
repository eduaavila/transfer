﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace HDH
{
    public class BackGroundBehaviour : MonoBehaviour
    {
        public GameObject hero;
        private KnightBehavior hc { get; set; }
        public GameObject bgDungeon;
        private string heroFacingPosition { get; set; }
        private float bgSpeed { get; set; }
        private Vector3 initialPosition { get; set; }
        private Vector3 positionLimit;

        // Start is called before the first frame update
        void Start()
        {
            Debug.Log("Background created!");
            initialPosition = transform.localPosition;
            bgSpeed = 200f;
            hero = GameObject.Find("Knight");
            hc = hero.GetComponent<KnightBehavior>();
            if (hero != null)
            {
                heroFacingPosition = hc.GetFacingDirection();
            }
            else
                heroFacingPosition = "facingRight";
            positionLimit = transform.Find("slice1").transform.localPosition;
        }

        // Update is called once per frame
        void Update()
        {
            heroFacingPosition = hc.GetFacingDirection();

            if(heroFacingPosition == "facingRight")
            {
                if (hc.GetIsMoving())
                {
                    transform.Translate(new Vector3(((bgSpeed * Time.deltaTime) * -1), 0, 0));
                }
            }
            else
            {
                if (hc.GetIsMoving())
                {
                    transform.Translate(new Vector3(((bgSpeed * Time.deltaTime)), 0, 0));
                }
            }

            if (transform.localPosition.x <= positionLimit.x)
            {
                transform.localPosition = initialPosition;
            }else if (transform.localPosition.x >= positionLimit.x*-1)
            {
                transform.localPosition = initialPosition;
            }
        }
    }
}