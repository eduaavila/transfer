﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HDH
{
    public class GoalCollider : MonoBehaviour
    {
        void OnTriggerEnter2D(Collider2D col)
        {
            CrossSceneClass.Log("Enigma solved.");
            string toLoad = "ConquerMoveScene";
            SceneManager.LoadScene(toLoad);
        }
    }
}