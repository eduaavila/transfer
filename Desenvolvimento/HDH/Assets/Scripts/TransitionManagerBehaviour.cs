﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HDH
{
    public class TransitionManagerBehaviour : MonoBehaviour
    {
        public GameObject transitionFade;
        private TransitionFadeBehaviour tfb;
        public GameObject transtionLightning;
        private TransitionLightningBehaviour tlb;

        // Start is called before the first frame update
        void Start()
        {
            //transitionFade = (GameObject)Resources.Load("/Resources/Prefabs/TransitionFade", typeof(GameObject)); //supostamente deveria conseguir carregar...
            //transitionFade = (GameObject)Resources.Load("/Resources/Prefabs/TransitionLightning", typeof(GameObject));
        }

        // TODO Remover metodo update assim que testes desta classe terminarem
        void Update()
        {
            if (Input.GetKeyDown("space"))
            {
                Debug.Log("Forced Instantiate of a FadeTransition!");
                Transition("", "transtionLightning", 1f, 1f);
            }
        }
        public void Transition(string sceneToLoad="", string transitionName = "transitionFade", float transitionDuration = 1f, float initialOpacity = 0.1f, float transitionSpeed=1f)
        {
            GameObject ChoosenTransition = new GameObject();
            switch (transitionName)
            {
                case "transitionFade":
                    ChoosenTransition = Instantiate(transitionFade, new Vector3(0, 0, 0), Quaternion.identity);
                    tfb = ChoosenTransition.GetComponent<TransitionFadeBehaviour>();
                    tfb.DefineSceneToLoad(sceneToLoad);
                    ChoosenTransition.transform.SetParent(transform.parent, false);
                    tfb.SetInitialOpacity(initialOpacity);
                    tfb.SetTransitionSpeed(transitionSpeed);
                    tfb.SetDuration(transitionDuration);
                    break;
                case "transtionLightning":
                    ChoosenTransition = Instantiate(transtionLightning, new Vector3(0, 0, 0), Quaternion.identity);
                    tlb = ChoosenTransition.GetComponent<TransitionLightningBehaviour>();
                    tlb.DefineSceneToLoad(sceneToLoad);
                    ChoosenTransition.transform.SetParent(transform.parent, false);
                    tlb.SetInitialOpacity(initialOpacity);
                    tlb.SetDuration(transitionDuration);
                    break;
            }

        }
    }

}