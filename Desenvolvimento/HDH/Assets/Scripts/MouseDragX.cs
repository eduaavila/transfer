﻿using Assets.Scripts.Models;
using UnityEngine;

namespace HDH
{
    public class MouseDragX : MouseDrag
    {
        private void Start()
        {
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
        }
    }
}