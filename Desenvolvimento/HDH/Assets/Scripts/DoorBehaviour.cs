﻿using OverlayControls;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HDH
{
    public class DoorBehaviour : MonoBehaviour
    {
        private void Start()
        {
            PauseBehaviour.GlobalizeUI();
            CrossSceneClass.SetFreezeCounter(true);
        }
        public void PassLock()
        {
            MapBehaviour.MovedFromDoor();
            CrossSceneClass.SetFreezeCounter(false);
            CrossSceneClass.Log("Passed through lock", SceneManager.GetActiveScene().name);
        }
    }
}