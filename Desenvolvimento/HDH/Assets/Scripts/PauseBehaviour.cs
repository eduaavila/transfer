﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace HDH
{
    public class PauseBehaviour : MonoBehaviour
    {
        private GameObject[] pauseObjs;
        private static bool isPaused;
        private static bool volumeChanged = false;
        private static bool sceneStarted = true;
        private AudioSource audioSrc;
        private GameObject dialogTrigger;

        void Start()
        {
            dialogTrigger = GameObject.FindGameObjectWithTag("Trigger");
            CrossSceneClass.Log("PauseBehaviour started");
            audioSrc = Camera.main.GetComponent<AudioSource>();
            audioSrc.volume = 0f;
            CrossSceneClass.Verbose("Initial volume set to 0");
            pauseObjs = GameObject.FindGameObjectsWithTag("Pause");
            CrossSceneClass.Verbose("Pause objects found");
            GlobalizeUI();
            foreach (GameObject pauseObj in pauseObjs)
            {
                if(pauseObj.name != "PauseObject")
                    pauseObj.SetActive(false);
            }
            PauseStart();
        }

        private static void PauseStart()
        {
            sceneStarted = true;
            volumeChanged = false;
            isPaused = false;
            CrossSceneClass.Verbose("PauseStart called");

        }
        private static void PauseUpdate()
        {
            volumeChanged = false;
            sceneStarted = false;
        }
        private static void SceneNotStarted()
        {
            sceneStarted = false;
        }

        void Update()
        {
            if (volumeChanged)
            {
                audioSrc.volume = CrossSceneClass.GetConfigs().GetVolumeMaster();
                PauseUpdate();
            }
            else if (sceneStarted)
            {
                if ((CrossSceneClass.GetConfigs().GetVolumeMaster() - audioSrc.volume) > 0.025f)
                    audioSrc.volume += (CrossSceneClass.GetConfigs().GetVolumeMaster() - audioSrc.volume) / 100;
                else
                {
                    audioSrc.volume = CrossSceneClass.GetConfigs().GetVolumeMaster();
                    SceneNotStarted();
                }
            }
            if (Input.GetKeyDown(KeyCode.Escape) && (SceneManager.GetActiveScene().name != "ExploreScene" && SceneManager.GetActiveScene().name != "EnigmaScene"))
            {
                if (isPaused)
                    Unpause();
                else
                    Pause();
            }
        }
        public void ChangeLanguage(string languageCode)
        {
            CrossSceneClass.GetConfigs().SetLanguage(languageCode);
            GlobalizeUI();
        }
        public void SlideVolume(float sliderVal)
        {
            CrossSceneClass.GetConfigs().SetVolumeMaster(sliderVal);
            SetVolumeChanged();
        }
        public void Pause()
        {
            CrossSceneClass.Log("Pause() called");
            SetPaused();
            Time.timeScale = 0;
            if (SceneManager.GetActiveScene().name == "DialogScene")
            {
                dialogTrigger.SetActive(false);
            }
            foreach (GameObject pauseObj in pauseObjs)
            {
                if (pauseObj.name == "sldVol")
                {
                    Slider sld = pauseObj.GetComponent<Slider>();
                    sld.value = CrossSceneClass.GetConfigs().GetVolumeMaster();
                }
                if (SceneManager.GetActiveScene().name == "DialogScene" && !pauseObj.name.Contains("ChangeLanguage"))
                    pauseObj.SetActive(true);
                else if (SceneManager.GetActiveScene().name != "DialogScene")
                    pauseObj.SetActive(true);
            }
        }
        public static void SetPaused()
        {
            isPaused = true;
        }
        public static void SetVolumeChanged()
        {
            volumeChanged = true;
        }
        public static void SetUnpaused()
        {
            isPaused = false;
        }

        public void Unpause()
        {
            SetUnpaused();
            Time.timeScale = 1;
            foreach (GameObject pauseObj in pauseObjs)
            {
                pauseObj.SetActive(false);
            }
            if (SceneManager.GetActiveScene().name == "DialogScene")
            {
                dialogTrigger.SetActive(true);
            }
            CrossSceneClass.GetConfigs().SaveConfigs();
        }
        public static void GlobalizeUI()
        {
            CrossSceneClass.Log("UI Globalized.", CrossSceneClass.GetConfigs().GetLanguage());
            foreach (Text t in FindObjectsOfType<Text>())
            {
                if (t.name.StartsWith("lbl"))
                {
                    t.text = CrossSceneClass.GetGlobalizedText(t.name);
                }
            }
        }

        public void SetPauseObjs(GameObject[] pauseObjs)
        {
            this.pauseObjs = pauseObjs;
        }

        public static bool IsPaused()
        {
            return isPaused;
        }
    }
}