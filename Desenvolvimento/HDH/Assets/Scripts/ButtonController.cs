﻿using UnityEngine;
using UnityEngine.UI;

namespace HDH
{
    public class ButtonController : MonoBehaviour
    {
        private string selectedObjectname;
        private bool btnTurnLeftSelected;
        private bool btnTurnRightSelected;
        private bool btnMoveSelected;

        public Button btnForward;
        public Button btnLeft;
        public Button btnRight;
        private KnightBehavior knb;


        // Start is called before the first frame update
        void Start()
        {
            btnMoveSelected = false;
            btnTurnRightSelected = false;
            btnTurnLeftSelected = false;

            GameObject knight = GameObject.Find("Knight");
            knb = knight.GetComponent<KnightBehavior>();
        }

        public string GetButtonPressed()
        {
            if (btnMoveSelected)
            {
                return "btnMoveSelected";
            }
            else if (btnTurnRightSelected)
            {
                return "btnTurnRightSelected";
            }
            else if (btnTurnLeftSelected)
            {
                return "btnTurnLeftSelected";
            }
            else return "";
        }

        public void Selected(GameObject sender)
        {
            selectedObjectname = sender.name;
            if(MapBehaviour.CanMoveForward())
            {
                switch (selectedObjectname)
                {
                    case "btnTurnLeft":
                        knb.FlipKnight();
                        btnTurnLeftSelected = true;
                        break;
                    case "btnTurnRight":
                        knb.FlipKnight();
                        btnTurnRightSelected = true;
                        break;
                    case "btnMove":
                        knb.SetIsMoving(true);
                        btnMoveSelected = true;
                        break;
                }
            }
        }

        // Update is called once per frame
        void Update()
        {
            btnMoveSelected = false;
            btnTurnRightSelected = false;
            btnTurnLeftSelected = false;

            if (knb.IsMoving())
            {
                btnForward.interactable = false;
                btnLeft.interactable = false;
                btnRight.interactable = false;
            }
            else
            {
                btnForward.interactable = true;
                btnLeft.interactable = true;
                btnRight.interactable = true;
            }

            selectedObjectname = "";
        }
    }
}