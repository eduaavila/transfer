﻿using HDH;
using OverlayControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class UnitFrameBehaviour : MonoBehaviour
    {
        private void OnMouseUp()
        {
            CrossSceneClass.Log("Unit frame clicked");
            if (GameObject.FindGameObjectsWithTag("Overlay").Length > 0) return;
            string query = MapBehaviour.GetUnitFound().GetUnitSpriteName().Split('_')[1];
            Overlay.LoadPropOverlayTextboxes(PropType.Unit, query);
            GameObject overlayGameObject = Instantiate(Resources.Load<GameObject>("Prefabs/Overlay"), GameObject.Find("Canvas").transform);
            overlayGameObject.name = "OverlayUnit";
            overlayGameObject.transform.localPosition.Set(0, 0, 0);
        }
    }
}
