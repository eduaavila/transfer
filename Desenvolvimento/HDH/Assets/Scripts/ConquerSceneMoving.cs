﻿using OverlayControls;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HDH
{
    public class ConquerSceneMoving : MonoBehaviour
    {
        void Start()
        {
            PauseBehaviour.GlobalizeUI();
            Time.timeScale = 0;
        }
        public void GoToNextConquerScene()
        {
            CrossSceneClass.Log("Moving to next conquer scene", CrossSceneClass.GetProfile().GetChapterInfo().GetStartingMap());
            Time.timeScale = 1;
            Returning(false);
            var o = GameObject.FindGameObjectsWithTag("ConquerMoving");
            TriggerType trigger;
            if (SceneManager.GetActiveScene().name == "ExploreScene")
                trigger = TriggerType.ExploreOnEnd; 
            else
                trigger = TriggerType.ConquerOnEnd;
            if (Overlay.TryLoadOverlayTextboxes(trigger, 0, 0))
            {
                CrossSceneClass.Log("Overlay Trigger loaded", trigger.ToString());
                GameObject overlayGameObject = Instantiate(Resources.Load<GameObject>("Prefabs/Overlay"), GameObject.Find("Canvas").transform);
                overlayGameObject.name = "OverlayOnEnd";
                overlayGameObject.transform.localPosition.Set(0, 0, 0);
            }
            else
            {
                if (SceneManager.GetActiveScene().name == "ConquerMoveScene")
                    CrossSceneClass.Cross();
                else
                    SceneManager.LoadScene("ConquerMoveScene");
            }
            foreach (GameObject obj in o)
            {
                obj.SetActive(false);
            }
        }
        public void ReturnToConquerMove()
        {
            CrossSceneClass.Log("Continuing to Explore", CrossSceneClass.GetProfile().GetChapterInfo().GetStartingMap());
            var o = GameObject.FindGameObjectsWithTag("ConquerMoving");
            foreach(GameObject obj in o)
            {
                Destroy(obj);
            }
            Returning(true);
            MapBehaviour.SetActiveHallwayButtons(true, true);
            Time.timeScale = 1;
        }
        private static void Returning(bool isReturning)
        {
            if (isReturning)
            {
                MapBehaviour.SetForceMoveBack(isReturning);
            }
            else
            {
                CrossSceneClass.SetReturningToMap(isReturning);
            }
        }
    }
}
