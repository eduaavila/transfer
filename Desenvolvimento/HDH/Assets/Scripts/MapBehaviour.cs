﻿using Assets.Scripts.Models;
using MapControls.Models;
using Newtonsoft.Json.Linq;
using OverlayControls;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace HDH
{
    public class MapBehaviour : MonoBehaviour
    {
        // GameMap
        public GameObject gameMap;

        // Prefabs
        public GameObject cellPrefab;
        public GameObject enemyIconPrefab;
        public GameObject exitIconPrefab;
		public GameObject doorIconPrefab;
	    public GameObject chestIconPrefab;
        public GameObject bossIconPrefab;

        // Static params
        private static DungeonMap currentMap;

        // Cells declare
        public float constSizeX;
        public float constSizeY;
        private float padX;
        private float padY;

        // Cursor declare
        private static int cursorDirection;
        private static int cursorX;
        private static int cursorY;
        private GameObject cursor;

        // Unit declare
        private GameObject unitObj;
        private EnemyUnit bossUnit;
        private List<int[]> emptyUnitCoordinates;        
        private static GameObject bossFound;
        private static EnemyUnit unitFound;

        // Chest declare
        private GameObject chestObj;
        private List<int[]> emptyChestCoordinates;
        private static Chest thisChest;

        // Door declare
        private static GameObject doorFound;

        // Others
        private static bool forceMoveBack;
        private static bool movementDisabled;
        private static bool recallMovement = false;
        
        void Start()
        {

            if (!CrossSceneClass.IsLoaded())
            {
                CrossSceneClass.Log($"File is not loaded.");
                SceneManager.LoadScene("MainMenuScene");
                return;
            }
            else if (SceneManager.GetActiveScene().name == "ExploreScene")
            {
                bossUnit = null;
                doorFound = null;
                bossFound = null;
                SetForceMoveBack(false);
                string load = CrossSceneClass.GetProfile().GetChapterInfo().GetStartingMap();
                SetCurrentMap(CrossSceneClass.GetMapping().maps[load]);
                CrossSceneClass.Log("Exploration Started", currentMap.GetName());
                List<EnemyUnit> units = new List<EnemyUnit>();
                EnemyUnit unit;
                string unitsString = currentMap.GenerateEntities();
                CrossSceneClass.Verbose("Generated entities string", $"unitsString:{unitsString}");
                int[] unitPos;
                string[] unitsStrings = unitsString.Split(';');
                emptyUnitCoordinates = new List<int[]>();
                for (int i=0; i < currentMap.tileMap.Count; i++)
                {
                    List<int> line = currentMap.tileMap[i];
                    for (int j = 0; j < line.Count; j++)
                    {
                        if(line[j] == 6)
                            emptyUnitCoordinates.Add(new int[]{ j, i});
                    }
                }
                foreach (string unitString in unitsStrings)
                {
                    CrossSceneClass.Log("emptyUnitCoordinates filled", $"{emptyUnitCoordinates.Count} empty enemy tiles");
                    unitPos = GetRandomEnemySpawn();
                    if (unitPos.Length == 0)
                    {
                        CrossSceneClass.Log("Could not get unchecked enemy spawn for unitString", unitString);
                    }
                    else
                    {
                        CrossSceneClass.Verbose("Preparing to spawn unit", unitString);
                        unit = new EnemyUnit(unitPos[1], unitPos[0]);
                        for (int i = 0; i < int.Parse(unitString.Split(':')[1]); i++)
                        {
                            unit.AddEnemy(new Enemy(Enemy.GenerateEnemyCode(currentMap.GetName(), unitString.Split(':')[0])));
                        }
                        currentMap.tileMap[unit.GetPosY()][unit.GetPosX()] = 7;
                        units.Add(unit);
                        CrossSceneClass.Log($"Spawned unit at ({unit.GetPosY()},{unit.GetPosX()})", $"{unitPos[2]} tries");
                    }
                }
                CrossSceneClass.SetActiveEnemies(units);
                // Get map chests
                List<Chest> chests = new List<Chest>();
                JArray mapChests = CrossSceneClass.GetProfile().GetChapterInfo().GetChestInfo()["map" + CrossSceneClass.GetProfile().GetChapterInfo().GetMapIndex()] as JArray;
                int[] chestPos;
                emptyChestCoordinates = new List<int[]>();
                for (int i = 0; i < currentMap.tileMap.Count; i++)
                {
                    List<int> line = currentMap.tileMap[i];
                    for (int j = 0; j < line.Count; j++)
                    {
                        if (line[j] == 3)
                            emptyChestCoordinates.Add(new int[] { j, i });
                    }
                }
                foreach (JToken chest in mapChests)
                {
                    chestPos = GetRandomChestSpawn();
                    chests.Add(new Chest(chest as JObject, chestPos[1], chestPos[0]));
                    CrossSceneClass.Log($"Spawned chest at ({chestPos[0]},{chestPos[1]})", $"{chestPos[2]} tries");
                }
                CrossSceneClass.SetActiveChests(chests);
                // Clean placeholder objects
                unitObj = GameObject.Find("UnitSprite");
                unitObj.SetActive(false);
                chestObj = GameObject.Find("ChestSprite");
                chestObj.SetActive(false);
            }
            else
            {
                SetCurrentMap(CrossSceneClass.GetCurrentMap());
                if (currentMap == null)
                {
                    string load = CrossSceneClass.GetProfile().GetChapterInfo().GetStartingMap();
                    SetCurrentMap(CrossSceneClass.GetMapping().maps[load]);
                }
                currentMap.revealedMap = CrossSceneClass.GetProfile().GetChapterInfo().GetRevealedMap();
                if (Overlay.TryLoadOverlayTextboxes(TriggerType.ConquerOnStart, 0, 0) && !CrossSceneClass.IsReturningToMap())
                {
                    CrossSceneClass.Log("Overlay Trigger loaded", TriggerType.ExploreOnStart.ToString());
                    GameObject overlayGameObject = Instantiate(Resources.Load<GameObject>("Prefabs/Overlay"), GameObject.Find("Canvas").transform);
                    overlayGameObject.name = "OverlayConquerOnStart";
                    overlayGameObject.transform.localPosition.Set(0, 0, 0);
                }
            }

            cursor = gameMap.transform.Find("mapCursor").gameObject;
            padX = constSizeX / currentMap.tileMap.Count;
            padY = constSizeY / currentMap.tileMap.Count;

            Vector3 moveVector = new Vector3(250,275,0);
            moveVector.Set((moveVector.x / currentMap.tileMap.Count), (moveVector.y / currentMap.tileMap.Count), 1);
            cursor.transform.localScale = moveVector;
            int[] sCoord = { 0, 0 };
            if (CrossSceneClass.IsReturningToMap())
            {
                sCoord[0] = CrossSceneClass.GetLastCursorPosY();
                sCoord[1] = CrossSceneClass.GetLastCursorPosX();
                SetCursorDirection(CrossSceneClass.GetLastCursorDirection());
                CrossSceneClass.Log("Entered Map", $"[{SceneManager.GetActiveScene().name}] X:{sCoord[1]} Y:{sCoord[0]} R: {cursorDirection}");
            }
            else
            {
                sCoord = currentMap.GetStartPosition();
                SetCursorDirection(CrossSceneClass.GetProfile().GetChapterInfo().GetStartingCursorDirection());
                CrossSceneClass.Log("Entered Map", $"[{SceneManager.GetActiveScene().name}] X:{sCoord[1]} Y:{sCoord[0]} R: {cursorDirection}");
            }
            for (int i = 0; i < currentMap.tileMap.Count; i++)
            {
                for (int j = 0; j < currentMap.tileMap.Count; j++)
                {
                    if (i == sCoord[0] && j == sCoord[1])
                    {
                        SetCursorX(j);
                        SetCursorY(i);
                        moveVector = new Vector3(0, 0, 0);
                        float posY = constSizeY / 2;
                        posY -= padY / 2;
                        posY -= i * padY;
                        float posX = -constSizeX / 2;
                        posX += padX / 2;
                        posX += j * padX;
                        moveVector.Set(posX, posY, 0);
                        cursor.transform.localPosition = moveVector;
                    }
                }
            }
            var cursorRotation = cursor.transform.localRotation;
            cursorRotation.eulerAngles = new Vector3(0, 0, cursorDirection);
            cursor.transform.localRotation = cursorRotation;

            bossUnit = GetMapBossUnit();
            if (bossUnit != null)
                CrossSceneClass.GetActiveEnemies().Add(bossUnit);

            if (SceneManager.GetActiveScene().name == "ExploreScene")
                currentMap.RevealFOVTiles(cursorY, cursorX, cursorDirection);
            UpdateMap();
            movementDisabled = false;
        }

        // Update is called once per frame
        void Update()
        {
            if (forceMoveBack)
            {
                SetForceMoveBack(false);
                Explore_MoveBack();
            }
            if (recallMovement)
            {
                recallMovement = false;
                Invoke("EnableMovement", .8f);
            }
        }

        private EnemyUnit GetMapBossUnit()
        {
            EnemyUnit bossUnit = null;
            if (currentMap.tileMap.Exists(x => x.Contains(8)))
            {
                int y = currentMap.tileMap.IndexOf(currentMap.tileMap.Find(line => line.Contains(8)));
                int x = currentMap.tileMap[y].IndexOf(currentMap.tileMap[y].Find(col => col == 8));
                CrossSceneClass.Verbose("Boss tile found", $"X:{x} Y:{y}");
                bossUnit = new EnemyUnit(x, y, true);
                currentMap.tileMap[y][x] = 7;
                
            }
            else
            {
                CrossSceneClass.Verbose("Boss tile not found", currentMap.GetName());
            }
            return bossUnit;
        }
	    private int[] GetRandomChestSpawn()
	    {
            List<int> position = new List<int>();
            int listIndex = Random.Range(0, emptyChestCoordinates.Count);
            int posY = emptyChestCoordinates[listIndex][1];
            int posX = emptyChestCoordinates[listIndex][0];
            position.Add(posY);
            position.Add(posX);
            position.Add(1);
            emptyChestCoordinates.RemoveAt(listIndex);
            return position.ToArray();
        }
        private int[] GetRandomEnemySpawn()
        {
            List<int> position = new List<int>();
            int listIndex = Random.Range(0, emptyUnitCoordinates.Count);
            int posY = emptyUnitCoordinates[listIndex][1];
            int posX = emptyUnitCoordinates[listIndex][0];
            position.Add(posY);
            position.Add(posX);
            position.Add(1);
            emptyUnitCoordinates.RemoveAt(listIndex);
            return position.ToArray();
        }
        public static EnemyUnit GetEnemyUnitAt(int posY, int posX)
        {
            return CrossSceneClass.GetActiveEnemies().Find(unit =>
                unit.GetPosX() == posX && unit.GetPosY() == posY
            );
        }
	    public static Chest GetChestAt(int posY, int posX)
	    {
	        return CrossSceneClass.GetActiveChests().Find(chest =>
	            chest.GetPosX() == posX && chest.GetPosY() == posY
	        );
	    }
        public void UpdateMap(bool checkTile = true)
        {
            float padXUpd = constSizeX / currentMap.tileMap.Count;
            float padYUpd = constSizeY / currentMap.tileMap.Count;

            Vector3 moveVector = new Vector3(280, 300, 0);
            moveVector.Set((moveVector.x / currentMap.tileMap.Count), (moveVector.y / currentMap.tileMap.Count), 1);

            Vector3 sizeVector = new Vector3(0, 0, 1);
            sizeVector.Set((constSizeX / currentMap.tileMap.Count) - 5, (constSizeY / currentMap.tileMap.Count) - 5, 1);
            CleanCells();
            for (int i = 0; i < currentMap.tileMap.Count; i++)
            {
                for (int j = 0; j < currentMap.tileMap.Count; j++)
                {
                    if (currentMap.revealedMap[i][j] != 0)
                    {
                        moveVector = new Vector3(0, 0, 0);
                        float posY = constSizeY / 2;
                        posY -= padYUpd / 2;
                        posY -= i * padYUpd;
                        float posX = -constSizeX / 2;
                        posX += padXUpd / 2;
                        posX += j * padXUpd;
                        moveVector.Set(posX, posY, 0);
                        var newCell = Instantiate(cellPrefab, gameMap.transform);
                        newCell.transform.localScale = sizeVector;
                        newCell.transform.localPosition = moveVector;
                    }
                }
            }
            if (cursorDirection == 180)
            {
                CrossSceneClass.GetProfile().GetChapterInfo().SetFacingRight(false);
            }
            else if (cursorDirection == 0)
            {
                CrossSceneClass.GetProfile().GetChapterInfo().SetFacingRight(true);
            }
            CrossSceneClass.SetCurrentMap(currentMap);
            CrossSceneClass.GetProfile().GetChapterInfo().SetRevealedMap(currentMap.revealedMap);

            if (SceneManager.GetActiveScene().name == "ConquerMoveScene" && checkTile)
            {
                if (Overlay.CanLoadTriggerTextboxes(TriggerType.ConquerOnTile, cursorX, cursorY))
                {
                    CrossSceneClass.Log("Overlay Trigger on tile loaded", TriggerType.ConquerOnTile.ToString());
                    GameObject overlayGameObject = Instantiate(Resources.Load<GameObject>("Prefabs/Overlay"), GameObject.Find("Canvas").transform);
                    overlayGameObject.name = "OverlayConquerOnTile";
                    overlayGameObject.transform.localPosition.Set(0, 0, 0);
                    Time.timeScale = 0;
                }
                if (currentMap.tileMap[cursorY][cursorX] == 2)
                {
                    CrossSceneClass.Log("Door tile found.", SceneManager.GetActiveScene().name);
                    currentMap.tileMap[cursorY][cursorX] = 1;
                    CrossSceneClass.SetLastCursorDirection(cursorDirection);
                    CrossSceneClass.SetLastCursorPosX(cursorX);
                    CrossSceneClass.SetLastCursorPosY(cursorY);
                    CrossSceneClass.SetReturningToMap(true);
                    EnigmaBehaviour.EnableTimeLimit(false);
                    CrossSceneClass.Log("Proceeding to EnigmaScene.", $"Type:Door Last X:{CrossSceneClass.GetLastCursorPosX()} Y:{CrossSceneClass.GetLastCursorPosY()} R:{CrossSceneClass.GetLastCursorDirection()}");
                    Invoke("LoadEnigmaScene", .75f);
                }
                else if (currentMap.tileMap[cursorY][cursorX] == 3)
                {
                    CrossSceneClass.Log("Chest tile found.", SceneManager.GetActiveScene().name);
                    currentMap.tileMap[cursorY][cursorX] = 1;
                    CrossSceneClass.SetLastCursorDirection(cursorDirection);
                    CrossSceneClass.SetLastCursorPosX(cursorX);
                    CrossSceneClass.SetLastCursorPosY(cursorY);
                    CrossSceneClass.SetReturningToMap(true);
                    EnigmaBehaviour.EnableTimeLimit(true);
                    CrossSceneClass.Log("Proceeding to EnigmaScene.", $"Type:Chest Last X:{CrossSceneClass.GetLastCursorPosX()} Y:{CrossSceneClass.GetLastCursorPosY()} R:{CrossSceneClass.GetLastCursorDirection()}");
                    Invoke("LoadEnigmaScene", .75f);
                }
                else if (currentMap.tileMap[cursorY][cursorX] == 5)
                {
                    CrossSceneClass.Log("Ending tile found.", SceneManager.GetActiveScene().name);
                    Instantiate(Resources.Load<GameObject>(@"Prefabs/PanelMoving"), GameObject.Find("Canvas").transform);
                }
                else if (currentMap.tileMap[cursorY][cursorX] == 7)
                {
                    EnemyUnit encounter = GetEnemyUnitAt(cursorY, cursorX);
                    if (encounter != null)
                    {
                        if (encounter.IsAlive())
                        {
                            CrossSceneClass.SetEncounter(encounter);
                            CrossSceneClass.SetLastCursorDirection(cursorDirection);
                            CrossSceneClass.SetLastCursorPosX(cursorX);
                            CrossSceneClass.SetLastCursorPosY(cursorY);
                            CrossSceneClass.SetReturningToMap(true);
                            CrossSceneClass.Log("Proceeding to Battle.", $"Last X:{CrossSceneClass.GetLastCursorPosX()} Y:{CrossSceneClass.GetLastCursorPosY()} R:{CrossSceneClass.GetLastCursorDirection()}");
                            Invoke("LoadBattleScene", .75f);
                        }
                    }
                    else
                    {
                        CrossSceneClass.Log("Map tile is 7 but encounter returned null", SceneManager.GetActiveScene().name);
                    }
                }
            }
            else if (SceneManager.GetActiveScene().name == "ExploreScene")
            {
                // Destroy extra objects
                if (unitObj.activeSelf)
                {
                    unitObj.SetActive(false);
                }
                if (chestObj.activeSelf)
                {
                    chestObj.SetActive(false);
                }
                if (Overlay.CanLoadTriggerTextboxes(TriggerType.ExploreOnTile, cursorX, cursorY))
                {
                    CrossSceneClass.Log("Overlay Trigger on tile loaded", TriggerType.ExploreOnTile.ToString());
                    GameObject overlayGameObject = Instantiate(Resources.Load<GameObject>("Prefabs/Overlay"), GameObject.Find("Canvas").transform);
                    if (currentMap.tileMap[cursorY][cursorX] == 2)
                        overlayGameObject.name = "OverlayDoorOnTile";
                    else
                        overlayGameObject.name = "OverlayOnTile";
                    overlayGameObject.transform.localPosition.Set(0, 0, 0);
                }
                // Check tile to instantiate extra objects
                if (currentMap.tileMap[cursorY][cursorX] == 2) // Door tile
                {
                    CrossSceneClass.Log("Door tile found.", SceneManager.GetActiveScene().name);
                    Transform playScreen = GameObject.Find("playScreen").transform;
                    SetDoorFound(Instantiate(Resources.Load<GameObject>(@"Prefabs/doorPrefab"), playScreen));
                    SetActiveHallwayButtons(false);
                    string objName = $"door{cursorY}_{cursorX}";
                    if (GameObject.Find(objName) == null)
                    {
                        var newDoor = Instantiate(doorIconPrefab, gameMap.transform);
                        newDoor.name = objName;
                        sizeVector = new Vector3(0, 0, 1);
                        sizeVector.Set((constSizeX / currentMap.tileMap.Count) - 5, (constSizeY / currentMap.tileMap.Count) - 5, 1);
                        newDoor.transform.localScale = sizeVector;
                        newDoor.transform.localPosition = cursor.transform.localPosition;
                    }
                }
                else if (currentMap.tileMap[cursorY][cursorX] == 3)
                {
                    CrossSceneClass.Log("Chest found", "common");
                    chestObj.SetActive(true);
                    CrossSceneClass.Log("Chest tile found.", SceneManager.GetActiveScene().name);
                    thisChest = GetChestAt(cursorY, cursorX);
                    string newObjName = $"chest{cursorY}_{cursorX}";
                    if (GameObject.Find(newObjName) == null)
                    {
                        var newChest = Instantiate(chestIconPrefab, gameMap.transform);
                        newChest.name = newObjName;
                        sizeVector = new Vector3(0, 0, 1);
                        sizeVector.Set((constSizeX / currentMap.tileMap.Count) - 5, (constSizeY / currentMap.tileMap.Count) - 5, 1);
                        newChest.transform.localScale = sizeVector;
                        newChest.transform.localPosition = cursor.transform.localPosition;
                    }
                }
                else if (currentMap.tileMap[cursorY][cursorX] == 5) // Ending tile
                {
                    CrossSceneClass.Log("Ending tile found.", SceneManager.GetActiveScene().name);
                    if (GameObject.Find("exitIcon") == null)
                    {
                        var newEnemy = Instantiate(exitIconPrefab, gameMap.transform);
                        newEnemy.name = "exitIcon";
                        sizeVector = new Vector3(0, 0, 1);
                        sizeVector.Set((constSizeX / currentMap.tileMap.Count) - 5, (constSizeY / currentMap.tileMap.Count) - 5, 1);
                        newEnemy.transform.localScale = sizeVector;
                        newEnemy.transform.localPosition = cursor.transform.localPosition;
                    }
                    Instantiate(Resources.Load<GameObject>(@"Prefabs/PanelExit"), gameMap.GetComponentInParent<Transform>());
                    SetActiveHallwayButtons(false, true);
                }
                else if(currentMap.tileMap[cursorY][cursorX] == 7) // Enemy tile
                {
                    unitFound = GetEnemyUnitAt(cursorY, cursorX);
                    if (unitFound != null)
                    {
                        if (unitFound.IsBossFight())
                        {
                            Transform playScreen = GameObject.Find("playScreen").transform;
                            SetBossFound(Instantiate(Resources.Load<GameObject>(@"Prefabs/bossFramePrefab"), playScreen));
                            SetActiveHallwayButtons(false);
                            string objName = "bossIcon";
                            if (GameObject.Find(objName) == null)
                            {
                                var bossIcon = Instantiate(bossIconPrefab, gameMap.transform);
                                bossIcon.name = objName;
                                sizeVector = new Vector3(0, 0, 1);
                                sizeVector.Set((constSizeX / currentMap.tileMap.Count) - 5, (constSizeY / currentMap.tileMap.Count) - 5, 1);
                                bossIcon.transform.localScale = sizeVector;
                                bossIcon.transform.localPosition = cursor.transform.localPosition;
                            }
                        }
                        else
                        {
                            string unitSpriteName = unitFound.GetUnitSpriteName();
                            CrossSceneClass.Log("Enemy unit found", unitSpriteName);
                            Sprite unitSprite = Resources.Load<Sprite>($"Sprites/dgSprite/{unitSpriteName}");
                            unitObj.GetComponent<SpriteRenderer>().sprite = unitSprite;
                            unitObj.SetActive(true);
                            string objName = $"enemy{cursorY}_{cursorX}";
                            if (GameObject.Find(objName) == null)
                            {
                                var newEnemy = Instantiate(enemyIconPrefab, gameMap.transform);
                                newEnemy.name = objName;
                                sizeVector = new Vector3(0, 0, 1);
                                sizeVector.Set((constSizeX / currentMap.tileMap.Count) - 5, (constSizeY / currentMap.tileMap.Count) - 5, 1);
                                newEnemy.transform.localScale = sizeVector;
                                newEnemy.transform.localPosition = cursor.transform.localPosition;
                            }
                        }
                    }
                }
            }
        }
        private void LoadBattleScene()
        {
            CrossSceneClass.Log("Entering battle scene", $"Unit: {CrossSceneClass.GetEncounter().GetUnitSpriteName()}");
	        SceneManager.LoadScene("ConquerBattleScene");
        }
        private void LoadEnigmaScene()
        {
            SceneManager.LoadScene("EnigmaScene");
        }

        public static void SetActiveHallwayButtons(bool active, bool affectBtnBack=false)
	    {
	        if(active && doorFound != null)
	        {
	            Destroy(doorFound);
	            doorFound = null;
	            CrossSceneClass.Log("doorFound", "null");
	        }
	        foreach (GameObject go in GameObject.FindGameObjectsWithTag("ExploreButton"))
	
	        {
	            CrossSceneClass.Verbose("Changing active status in button", go.name + " " + active);
	            go.GetComponent<Button>().interactable = active;
	
	        }
	        if (affectBtnBack)
	
	        {
	            CrossSceneClass.Verbose("Changing active status in button", "btnBack " + active);
                if(GameObject.Find("btnBack") != null)
	                GameObject.Find("btnBack").GetComponent<Button>().interactable = active;
	        }
	    }

        private bool LoyaltyCheck()
        {
            CrossSceneClass.SetCurrentMap(currentMap);
            float completionRate = float.Parse((CrossSceneClass.GetCurrentMap().GetCompletionRate() * 100).ToString());
            CrossSceneClass.GetProfile().GetHero().UpdateLoyalty(completionRate);
            return CrossSceneClass.GetProfile().GetHero().IsLoyal();
        }

        public void RandomMovement(string dir)
        {
            movementDisabled = true;
            gameMap.GetComponent<Image>().color = Color.red;
            int random = Random.Range(00, 02);
            switch (dir)
            {
                case "R":
                    if (random == 0) TurnLeftCore();
                    else MoveForwardCore();
                    break;
                case "L":
                    if (random == 0) TurnRightCore();
                    else MoveForwardCore();
                    break;
                case "F":
                    if (random == 0) TurnLeftCore();
                    else TurnRightCore();
                    break;
            }
            Invoke("EnableMovement", .8f);
        }

        public void EnableMovement()
        {
            movementDisabled = false;
            gameMap.GetComponent<Image>().color = Color.white;
        }

        private void TurnRightCore()
        {
            SetCursorDirection((cursorDirection + 270) % 360);
            var cursorRotation = cursor.transform.localRotation;
            cursorRotation.eulerAngles = new Vector3(0, 0, cursorDirection);
            cursor.transform.localRotation = cursorRotation;
            currentMap.RevealFOVTiles(cursorY, cursorX, cursorDirection);
            UpdateMap(false);
        }
        public void TurnRight()
        {
            if (!movementDisabled)
            {
                if (LoyaltyCheck())
                {
                    TurnRightCore();
                }
                else
                {
                    RandomMovement("R");
                }
            }
        }

        private void TurnLeftCore()
        {
            SetCursorDirection((cursorDirection + 90) % 360);
            var cursorRotation = cursor.transform.localRotation;
            cursorRotation.eulerAngles = new Vector3(0, 0, cursorDirection);
            cursor.transform.localRotation = cursorRotation;
            currentMap.RevealFOVTiles(cursorY, cursorX, cursorDirection);
            UpdateMap(false);
        }
        public void TurnLeft()
        {
            if (!movementDisabled)
            {
                if (LoyaltyCheck())
                {
                    TurnLeftCore();
                }
                else
                {
                    RandomMovement("L");
                }
            }
        }
        public static bool CanMoveForward()
        {
            bool can = false;
            switch (cursorDirection)
            {
                case 0:
                    if (cursorX < currentMap.tileMap.Count - 1 && currentMap.tileMap[cursorY][cursorX + 1] != 0)
                    {
                        can = true;
                    }
                    break;
                case 90:
                    if (cursorY > 0 && currentMap.tileMap[cursorY - 1][cursorX] != 0)
                    {
                        can = true;
                    }
                    break;
                case 180:
                    if (cursorX > 0 && currentMap.tileMap[cursorY][cursorX - 1] != 0)
                    {
                        can = true;
                    }
                    break;
                case 270:
                    if (cursorY < currentMap.tileMap.Count - 1 && currentMap.tileMap[cursorY + 1][cursorX] != 0)
                    {
                        can = true;
                    }
                    break;
                default: throw new System.InvalidOperationException("Invalid cursorDirection");
            }
            return can;
        }

        private void MoveForwardCore()
        {
            Vector3 moveVector;
            bool moved = false;
            switch (cursorDirection)
            {
                case 0:
                    if (cursorX < currentMap.tileMap.Count - 1 && currentMap.tileMap[cursorY][cursorX + 1] != 0)
                    {
                        SetCursorX(cursorX+1);
                        moveVector = cursor.transform.localPosition;
                        moveVector.Set((moveVector.x + padX), moveVector.y, 1);
                        cursor.transform.localPosition = moveVector;
                        moved = true;
                    }
                    break;
                case 90:
                    if (cursorY > 0 && currentMap.tileMap[cursorY - 1][cursorX] != 0)
                    {
                        SetCursorY(cursorY-1);
                        moveVector = cursor.transform.localPosition;
                        moveVector.Set(moveVector.x, moveVector.y + padY, 1);
                        cursor.transform.localPosition = moveVector;
                        moved = true;
                    }
                    break;
                case 180:
                    if (cursorX > 0 && currentMap.tileMap[cursorY][cursorX - 1] != 0)
                    {
                        SetCursorX(cursorX-1);
                        moveVector = cursor.transform.localPosition;
                        moveVector.Set((moveVector.x - padX), moveVector.y, 1);
                        cursor.transform.localPosition = moveVector;
                        moved = true;
                    }
                    break;
                case 270:
                    if (cursorY < currentMap.tileMap.Count - 1 && currentMap.tileMap[cursorY + 1][cursorX] != 0)
                    {
                        SetCursorY(cursorY+1);
                        moveVector = cursor.transform.localPosition;
                        moveVector.Set(moveVector.x, moveVector.y - padY, 1);
                        cursor.transform.localPosition = moveVector;
                        moved = true;
                    }
                    break;
                default: throw new System.InvalidOperationException("Invalid cursorDirection");
            }
            if (moved)
            {
                currentMap.RevealFOVTiles(cursorY, cursorX, cursorDirection);
                if (SceneManager.GetActiveScene().name == "ExploreScene")
                    ExplorationBehaviour.UpdateSurrounds(cursorX, cursorY, cursorDirection);
                UpdateMap();
            }
        }

        public void Explore_MoveForward()
        {
            if (!movementDisabled)
            {
                if (SceneManager.GetActiveScene().name == "ExploreScene" || LoyaltyCheck())
                {
                    MoveForwardCore();
                    movementDisabled = true;
                    Invoke("EnableMovement", .8f);
                }
                else
                {
                    RandomMovement("F");
                }
            }
        }
        public void Explore_MoveRight()
        {
            Vector3 moveVector;
            bool moved = false;
            if (!movementDisabled)
            {
                switch (cursorDirection)
                {
                    case 0:
                        if (cursorY < currentMap.tileMap.Count - 1)
                        {
                            SetCursorY(cursorY + 1);
                            moveVector = cursor.transform.localPosition;
                            moveVector.Set(moveVector.x, moveVector.y - padY, 1);
                            cursor.transform.localPosition = moveVector;
                            moved = true;
                        }
                        break;
                    case 90:
                        if (cursorX < currentMap.tileMap.Count - 1)
                        {
                            SetCursorX(cursorX + 1);
                            moveVector = cursor.transform.localPosition;
                            moveVector.Set(moveVector.x + padX, moveVector.y, 1);
                            cursor.transform.localPosition = moveVector;
                            moved = true;
                        }
                        break;
                    case 180:
                        if (cursorY > 0)
                        {
                            SetCursorY(cursorY - 1);
                            moveVector = cursor.transform.localPosition;
                            moveVector.Set(moveVector.x, moveVector.y + padY, 1);
                            cursor.transform.localPosition = moveVector;
                            moved = true;
                        }
                        break;
                    case 270:
                        if (cursorX > 0)
                        {
                            SetCursorX(cursorX - 1);
                            moveVector = cursor.transform.localPosition;
                            moveVector.Set(moveVector.x - padX, moveVector.y, 1);
                            cursor.transform.localPosition = moveVector;
                            moved = true;
                        }
                        break;
                    default: throw new System.InvalidOperationException("Invalid cursorDirection");
                }
            }
            
            if (moved)
            {
                movementDisabled = true;
                Invoke("EnableMovement", .8f);
                SetCursorDirection((cursorDirection + 270) % 360);
                var cursorRotation = cursor.transform.localRotation;
                cursorRotation.eulerAngles = new Vector3(0, 0, cursorDirection);
                cursor.transform.localRotation = cursorRotation;
                currentMap.RevealFOVTiles(cursorY, cursorX, cursorDirection);
                if (SceneManager.GetActiveScene().name == "ExploreScene")
                    ExplorationBehaviour.UpdateSurrounds(cursorX, cursorY, cursorDirection);
                UpdateMap();
            }
        }
        public void Explore_MoveLeft()
        {
            Vector3 moveVector;
            bool moved = false;

            if (!movementDisabled)
            {
                switch (cursorDirection)
                {
                    case 0:
                        if (cursorY > 0)
                        {
                            SetCursorY(cursorY - 1);
                            moveVector = cursor.transform.localPosition;
                            moveVector.Set(moveVector.x, moveVector.y + padY, 1);
                            cursor.transform.localPosition = moveVector;
                            moved = true;
                        }
                        break;
                    case 90:
                        if (cursorX > 0)
                        {
                            SetCursorX(cursorX - 1);
                            moveVector = cursor.transform.localPosition;
                            moveVector.Set(moveVector.x - padX, moveVector.y, 1);
                            cursor.transform.localPosition = moveVector;
                            moved = true;
                        }
                        break;
                    case 180:
                        if (cursorY < currentMap.tileMap.Count - 1)
                        {
                            SetCursorY(cursorY + 1);
                            moveVector = cursor.transform.localPosition;
                            moveVector.Set(moveVector.x, moveVector.y - padY, 1);
                            cursor.transform.localPosition = moveVector;
                            moved = true;
                        }
                        break;
                    case 270:
                        if (cursorX < currentMap.tileMap.Count - 1)
                        {
                            SetCursorX(cursorX + 1);
                            moveVector = cursor.transform.localPosition;
                            moveVector.Set(moveVector.x + padX, moveVector.y, 1);
                            cursor.transform.localPosition = moveVector;
                            moved = true;
                        }
                        break;
                    default: throw new System.InvalidOperationException("Invalid cursorDirection");
                }
            }

            if (moved)
            {
                movementDisabled = true;
                Invoke("EnableMovement", .8f);
                SetCursorDirection((cursorDirection + 90) % 360);
                var cursorRotation = cursor.transform.localRotation;
                cursorRotation.eulerAngles = new Vector3(0, 0, cursorDirection);
                cursor.transform.localRotation = cursorRotation;
                currentMap.RevealFOVTiles(cursorY, cursorX, cursorDirection);
                if (SceneManager.GetActiveScene().name == "ExploreScene")
                    ExplorationBehaviour.UpdateSurrounds(cursorX, cursorY, cursorDirection);
                UpdateMap();
            }
        }
        public void Explore_MoveBack()
        {
            Vector3 moveVector;
            bool moved = false;
            if (bossFound != null)
                MovedFromBoss();
            else if (doorFound != null)
                MovedFromDoor();
            if (!movementDisabled)
            {                                
                switch (cursorDirection)
                {
                    case 0:
                        if (cursorX > 0)
                        {
                            SetCursorX(cursorX - 1);
                            moveVector = cursor.transform.localPosition;
                            moveVector.Set((moveVector.x - padX), moveVector.y, 1);
                            cursor.transform.localPosition = moveVector;
                            moved = true;
                        }
                        break;
                    case 90:
                        if (cursorY < currentMap.tileMap.Count - 1)
                        {
                            SetCursorY(cursorY + 1);
                            moveVector = cursor.transform.localPosition;
                            moveVector.Set(moveVector.x, moveVector.y - padY, 1);
                            cursor.transform.localPosition = moveVector;
                            moved = true;
                        }
                        break;
                    case 180:
                        if (cursorX < currentMap.tileMap.Count - 1)
                        {
                            SetCursorX(cursorX + 1);
                            moveVector = cursor.transform.localPosition;
                            moveVector.Set((moveVector.x + padX), moveVector.y, 1);
                            cursor.transform.localPosition = moveVector;
                            moved = true;
                        }
                        break;
                    case 270:
                        if (cursorY > 0)
                        {
                            SetCursorY(cursorY - 1);
                            moveVector = cursor.transform.localPosition;
                            moveVector.Set(moveVector.x, moveVector.y + padY, 1);
                            cursor.transform.localPosition = moveVector;
                            moved = true;
                        }
                        break;
                    default: throw new System.InvalidOperationException("Invalid cursorDirection");
                }
            }
            
            if (moved)
            {
                movementDisabled = true;
                Invoke("EnableMovement", .8f);
                SetCursorDirection((cursorDirection + 180) % 360);
                var cursorRotation = cursor.transform.localRotation;
                cursorRotation.eulerAngles = new Vector3(0, 0, cursorDirection);
                cursor.transform.localRotation = cursorRotation;
                currentMap.RevealFOVTiles(cursorY, cursorX, cursorDirection);
                if (SceneManager.GetActiveScene().name == "ExploreScene")
                {
                    ExplorationBehaviour.UpdateSurrounds(cursorX, cursorY, cursorDirection);
                    EnemyUnit unitFound = GetEnemyUnitAt(cursorY, cursorX);
                    if (unitFound != null)
                    {
                        string unitSpriteName = unitFound.GetUnitSpriteName();
                        CrossSceneClass.Log("Enemy unit found", unitSpriteName);
                        Sprite unitSprite = Resources.Load<Sprite>($@"Sprites/dgSprite/{unitSpriteName}");
                        unitObj.GetComponent<SpriteRenderer>().sprite = unitSprite;
                        unitObj.SetActive(true);
                    }
                    else
                    {
                        unitObj.SetActive(false);
                    }
                }
                UpdateMap();
            }
        }
        public static void CleanCells()
        {
            foreach (GameObject o in Object.FindObjectsOfType<GameObject>())
            {
                if(o.name == "cellExplore(Clone)")
                {
                    Destroy(o);
                }
            }
        }

        public static bool IsForceMoveBack()
        {
            return forceMoveBack;
        }
        public static void SetForceMoveBack(bool force)
        {
            forceMoveBack = force;
        }
        public static DungeonMap GetCurrentMap()
        {
            return currentMap;
        }
        public static void SetCurrentMap(DungeonMap map)
        {
            currentMap = map;
        }
        public static int GetCursorDirection()
        {
            return cursorDirection;
        }
        public static void SetCursorDirection(int dir)
        {
            cursorDirection = dir;
        }
        public static int GetCursorX()
        {
            return cursorX;
        }
        public static void SetCursorX(int pos)
        {
            cursorX = pos;
        }
        public static int GetCursorY()
        {
            return cursorY;
        }
        public static void SetCursorY(int pos)
        {
            cursorY = pos;
        }
        public static void SetDoorFound(GameObject found)
        {
            doorFound = found;
            doorFound.name = "DoorPrefab";
        }
        public static void MovedFromDoor()
        {
            Time.timeScale = 1f;
            Destroy(GameObject.Find("DoorPrefab"));
            recallMovement = true;
            SetActiveHallwayButtons(true);
            doorFound = null;
        }
        public static void SetBossFound(GameObject found)
        {
            bossFound = found;
            bossFound.name = "BossFramePrefab";
        }
        public static void MovedFromBoss()
        {
            Time.timeScale = 1f;
            Destroy(GameObject.Find("BossFramePrefab"));
            recallMovement = true;
            SetActiveHallwayButtons(true);
            bossFound = null;
        }
        public static Chest GetThisChest()
        {
            return thisChest;
        }
        public static EnemyUnit GetUnitFound()
        {
            return unitFound;
        }
    }
}