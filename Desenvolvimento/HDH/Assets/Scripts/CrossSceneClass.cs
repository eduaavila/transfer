﻿using Assets.Scripts.Models;
using MapControls;
using MapControls.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HDH
{
    public static class CrossSceneClass
    {
        private static readonly bool logVerbose = true;

        private static bool loaded = false;
        private static bool willCreate = false;
        private static Configs configs = new Configs();
        private static PlayerProfile profile;
        private static Mapping mapping;
        private static DungeonMap currentMap;
        private static List<EnemyUnit> activeEnemies;
        private static List<Chest> activeChests;
        private static EnemyUnit encounter;
        private static string logFile = "";
        private static bool returningToMap = false;
        private static int lastCursorDirection;
        private static int lastCursorPosX;
        private static int lastCursorPosY;
        private static bool freezeCounters;

        private static TextAsset globalization;

        public static void LoadFile()
        {
            string path = Application.persistentDataPath + "/gamesave.txt";
            Log("Set gamesave path", path);
            mapping = null;
            currentMap = null;
            profile = new PlayerProfile();
            activeChests = new List<Chest>();
            activeEnemies = new List<EnemyUnit>();
            encounter = null;
            lastCursorDirection = 0;
            lastCursorPosX= 0;
            lastCursorPosY = 0;
            returningToMap = false;
            freezeCounters = false;
            if (File.Exists(path) && !willCreate)
            {
                Verbose("Preparing to load gamesave.txt");
                string toLoad = File.ReadAllText(path);
                Verbose("Chapter hash loaded", toLoad);
                var byteStream = Convert.FromBase64String(toLoad);
                string chapterToLoad = System.Text.Encoding.ASCII.GetString(byteStream);
                Verbose("Decoding hash", chapterToLoad);
                if (chapterToLoad == "ending")
                    chapterToLoad = "chapter1";
                profile.SetChapterInfo(new ChapterInfo(chapterToLoad, 0, 0));
            }
            else
            {
                Verbose("Creating new save file");
                profile.SetChapterInfo(new ChapterInfo());
                profile.SaveFile();
            }
            string mapInfoTxt = Resources.Load<TextAsset>(@"JSON/mapInfo").text;
            string generationSettingsTxt = Resources.Load<TextAsset>(@"JSON/generationSettings").text;
            mapping = new Mapping(mapInfoTxt, generationSettingsTxt);
            Log("Mapping set");
            profile.SetHero(new Hero("classic"));
            Log("Hero set", profile.GetHero().GetHeroType());
            loaded = true;
            Cross();
        }
        public static void Cross()
        {
            Log("Cross() called.", SceneManager.GetActiveScene().name);
            Time.timeScale = 1f;
            if (profile.GetChapterInfo().IsLastDialogIndex())
            {
                if (profile.GetChapterInfo().IsLastMapIndex())
                {
                    string newChapter = ChapterInfo.GetNextChapterName(profile.GetChapterInfo().GetChapterName());
                    if (newChapter != "ending")
                    {
                        profile.SetChapterInfo(new ChapterInfo(newChapter));
                        GetProfile().GetHero().SetHp(GetProfile().GetHero().GetBaseHp());
                        profile.SaveFile();
                        SceneManager.LoadScene("DialogScene");
                    }
                    else
                    {
                        Log("Demo ended.", "Thank you");
                        SceneManager.LoadScene("DialogScene");
                    }
                }
                else
                {
                    profile.SetChapterInfo(new ChapterInfo(profile.GetChapterInfo().GetChapterName(), profile.GetChapterInfo().GetMapIndex() + 1, profile.GetChapterInfo().GetDialogIndex()));
                    SceneManager.LoadScene("ExploreScene");
                }
            }
            else
            {
                profile.SetChapterInfo(new ChapterInfo(profile.GetChapterInfo().GetChapterName(), profile.GetChapterInfo().GetMapIndex(), profile.GetChapterInfo().GetDialogIndex() + 1));
                SceneManager.LoadScene("DialogScene");
            }
        }
        public static string GetGlobalizedText(string lblCode)
        {
            if (globalization == null)
                globalization = Resources.Load<TextAsset>(@"JSON/globalization");
            JObject labels = (JsonConvert.DeserializeObject(globalization.text) as JObject)["labels"] as JObject;
            return labels[lblCode][configs.GetLanguage()].ToString();
        }
        public static void Verbose(string text, string extraInfo = "")
        {
            if (!logVerbose) return;
            if (String.IsNullOrEmpty(logFile))
            {
                logFile = Application.persistentDataPath + $@"\logs\{DateTime.Now.ToString("yyyyMMdd_HHmmss")}.txt";
                logFile = logFile.Replace(@"\", "/");
                if (!Directory.Exists(Application.persistentDataPath + "/logs"))
                {
                    Directory.CreateDirectory(Application.persistentDataPath + "/logs");
                }
                var f = File.Create(logFile);
                f.Close();
            }
            string logHolder = DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss]");
            logHolder += $" [{SceneManager.GetActiveScene().name}] ";
            string textHold = new string(' ', 48 - logHolder.Length);
            logHolder += textHold;
            textHold = new string(' ', 80 - text.Length);
            logHolder += text + textHold;
            logHolder += extraInfo != "" ? $" [{extraInfo}]" : "";
            Debug.Log(logHolder);
            StreamWriter sw = File.AppendText(logFile);
            try
            {
                sw.WriteLine(logHolder);
            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }
            finally
            {
                sw.Close();
            }
        }
        public static void Log(string text, string extraInfo = "")
        {
            if (String.IsNullOrEmpty(logFile))
            {
                logFile = Application.persistentDataPath + $@"\logs\{DateTime.Now.ToString("yyyyMMdd_HHmmss")}.txt";
                logFile = logFile.Replace(@"\", "/");
                if (!Directory.Exists(Application.persistentDataPath + "/logs"))
                {
                    Directory.CreateDirectory(Application.persistentDataPath + "/logs");
                }
                var f = File.Create(logFile);
                f.Close();
            }
            string logHolder = DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss]");
            logHolder += $" [{SceneManager.GetActiveScene().name}] ";
            string textHold = new string(' ', 48 - logHolder.Length);
            logHolder += textHold;
            textHold = new string(' ', 80 - text.Length);
            logHolder += text + textHold;
            logHolder += extraInfo != "" ? $" [{extraInfo}]" : "";
            Debug.Log(logHolder);
            StreamWriter sw = File.AppendText(logFile);
            try
            {
                sw.WriteLine(logHolder);
            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }
            finally
            {
                sw.Close();
            }
        }

        public static bool IsLoaded()
        {
            return loaded;
        }
        public static void SetLoaded(bool load)
        {
            loaded = load;
        }
        public static bool WillCreate()
        {
            return willCreate;
        }
        public static void SetCreate(bool create)
        {
            willCreate = create;
        }
        public static bool IsReturningToMap()
        {
            return returningToMap;
        }
        public static void SetReturningToMap(bool returning)
        {
            returningToMap = returning;
        }
        public static Configs GetConfigs()
        {
            return configs;
        }
        public static void SetConfigs(Configs confs)
        {
            configs = confs;
        }
        public static PlayerProfile GetProfile()
        {
            return profile;
        }
        public static void SetProfile(PlayerProfile playerProfile)
        {
            profile = playerProfile;
        }
        public static Mapping GetMapping()
        {
            return mapping;
        }
        public static void SetMapping(Mapping map)
        {
            mapping = map;
        }
        public static DungeonMap GetCurrentMap()
        {
            return currentMap;
        }
        public static void SetCurrentMap(DungeonMap map)
        {
            currentMap = map;
        }
        public static List<EnemyUnit> GetActiveEnemies()
        {
            return activeEnemies;
        }
        public static void SetActiveEnemies(List<EnemyUnit> enemies)
        {
            activeEnemies = enemies;
        }
        public static List<Chest> GetActiveChests()
        {
            return activeChests;
        }
        public static void SetActiveChests(List<Chest> chests)
        {
            activeChests = chests;
        }
        public static EnemyUnit GetEncounter()
        {
            return encounter;
        }
        public static void SetEncounter(EnemyUnit enemy)
        {
            encounter = enemy;
        }
        public static int GetLastCursorDirection()
        {
            return lastCursorDirection;
        }
        public static void SetLastCursorDirection(int dir)
        {
            lastCursorDirection = dir;
        }
        public static int GetLastCursorPosX()
        {
            return lastCursorPosX;
        }
        public static void SetLastCursorPosX(int pos)
        {
            lastCursorPosX = pos;
        }
        public static int GetLastCursorPosY()
        {
            return lastCursorPosY;
        }
        public static void SetLastCursorPosY(int pos)
        {
            lastCursorPosY = pos;
        }
        public static void SetFreezeCounter(bool freeze)
        {
            freezeCounters = freeze;
        }
        public static bool MustFreezeCounters()
        {
            return freezeCounters;
        }
    }
}