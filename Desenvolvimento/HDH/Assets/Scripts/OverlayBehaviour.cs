﻿using OverlayControls.Models;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace HDH
{
    public class OverlayBehaviour : MonoBehaviour
    {
        private List<OverlayTextbox> textboxes;
        private int boxIndex;
        private GameObject refContent;
        private Transform panel;
        private string sceneName;
        private void Start()
        {
            textboxes = Overlay.GetCurrentOverlayTextboxes();
            sceneName = SceneManager.GetActiveScene().name;
            boxIndex = 0;
            if(boxIndex == textboxes.Count)
            {
                if (!gameObject.name.Contains("Door"))
                {
                    CrossSceneClass.SetFreezeCounter(false);
                    if (sceneName == "ExploreScene")
                    {
                        MapBehaviour.SetActiveHallwayButtons(true, true);
                    }
                    else if (sceneName == "ConquerMoveScene")
                    {
                        foreach(GameObject go in GameObject.FindGameObjectsWithTag("ConquerMoveButton"))
                        {
                            go.GetComponent<Button>().interactable = true;
                        }
                    }
                    if (gameObject.name.EndsWith("OnEnd"))
                    {
                        if (sceneName == "ExploreScene")
                        {
                            SceneManager.LoadScene("ConquerMoveScene");
                        }
                        else if(sceneName == "ConquerMoveScene")
                        {
                            CrossSceneClass.Cross();
                        }
                    }
                    else
                    {
                        Destroy(gameObject);
                    }
                }
                else
                {
                    Destroy(GameObject.Find("OverlayDoor"));
                    Destroy(GameObject.Find("OverlayDoorOnTile"));
                }
            }
            else
            {
                Transform canvas = transform.Find("OverlayCanvas");
                transform.Find("OverlayCanvas").lossyScale.Set(1, 1, 1);
                panel = canvas.transform.Find("OverlayPanel");
                UpdateTextbox(textboxes[0]);
                if (!gameObject.name.Contains("Door"))
                {
                    CrossSceneClass.SetFreezeCounter(true);
                    if (sceneName == "ExploreScene")
                    {
                        MapBehaviour.SetActiveHallwayButtons(false, true);
                    }
                    else if (sceneName == "ConquerMoveScene")
                    {
                        Time.timeScale = 0f;
                        foreach (GameObject go in GameObject.FindGameObjectsWithTag("ConquerMoveButton"))
                        {
                            go.GetComponent<Button>().interactable = false;
                        }
                    }
                }                
            }
        }
        private void UpdateTextbox(OverlayTextbox box)
        {
            float newY;
            Transform refBox = panel.transform.Find("OverlayBox");
            GameObject refSprite = refBox.Find("OverlaySprite").gameObject;
            GameObject refSpeaker = refBox.Find("OverlaySpeaker").gameObject;
            refContent = refBox.Find("OverlayContent").gameObject;
            switch (box.GetPosition())
            {
                case "bottom": newY = -440f; break;
                case "top": newY = 320f; break;
                default: newY = 0f; break;
            }
            Debug.Log(newY);
            if (newY != 0f)
                panel.transform.Find("OverlayBox").localPosition = new Vector3(refBox.localPosition.x, newY, refBox.localPosition.z);
            Debug.Log(panel.transform.Find("OverlayBox").localPosition);
            refSprite.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/VN/Overlay/" + box.GetSprite());
            refSpeaker.GetComponent<Text>().text = box.GetSpeaker();
            refContent.GetComponent<Text>().text = box.GetText();
        }
        public void NextTextbox()
        {
            boxIndex++;
            if (boxIndex == textboxes.Count)
            {
                CrossSceneClass.Log("Destroying overlay");
                if (!gameObject.name.Contains("Door"))
                {
                    CrossSceneClass.SetFreezeCounter(false);
                    if (sceneName == "ExploreScene")
                    {
                        MapBehaviour.SetActiveHallwayButtons(true, true);
                    }
                    else if (sceneName == "ConquerMoveScene")
                    {
                        Time.timeScale = 1f;
                        foreach (GameObject go in GameObject.FindGameObjectsWithTag("ConquerMoveButton"))
                        {
                            go.GetComponent<Button>().interactable = true;
                        }
                    }
                    if (gameObject.name.EndsWith("OnEnd"))
                    {
                        if (sceneName == "ExploreScene")
                        {
                            SceneManager.LoadScene("ConquerMoveScene");
                        }
                        else
                        {
                            CrossSceneClass.Cross();
                        }
                    }
                    else
                    {
                        Destroy(gameObject);
                    }
                }
                else
                {
                    Destroy(GameObject.Find("OverlayDoor"));
                    Destroy(GameObject.Find("OverlayDoorOnTile"));
                }
            }
            else
            {
                UpdateTextbox(textboxes[boxIndex]);
            }
        }
    }
}